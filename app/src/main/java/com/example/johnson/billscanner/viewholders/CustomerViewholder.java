package com.example.johnson.billscanner.viewholders;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.example.johnson.billscanner.R;
import com.example.johnson.billscanner.entities.CustomerEntity;
import com.example.johnson.billscanner.interfaces.Controller;
import com.example.johnson.billscanner.listeners.ChooseVendorsActionClickLIstener;

public class CustomerViewholder extends BaseViewHolder<CustomerEntity> {

    Controller controller;
    View view;
    TextView name;
    ChooseVendorsActionClickLIstener chooseVendorsActionClickLIstener;
    RelativeLayout relativeLayout;
    CustomerEntity entity;

    public CustomerViewholder(View itemView, Controller mController, ChooseVendorsActionClickLIstener chooseVendorsActionClickLIstener) {
        super(itemView);

        this.view = itemView;
        this.controller = mController;
        this.chooseVendorsActionClickLIstener = chooseVendorsActionClickLIstener;
        name = itemView.findViewById(R.id.itemTextView);
        relativeLayout = itemView.findViewById(R.id.rl_container);
        setActionListenre();
    }


    @Override
    public void bind(CustomerEntity entity) {
        this.entity = entity;

        name.setText(entity.getName());

    }



    private void setActionListenre(){

        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chooseVendorsActionClickLIstener != null)
                    chooseVendorsActionClickLIstener.onItemClick(entity);
            }
        });

    }
}