package com.example.johnson.billscanner.dialog;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;

import com.example.johnson.billscanner.R;
import com.example.johnson.billscanner.activities.TypeOfProcessActivity;
import com.example.johnson.billscanner.base.BaseDialog;
import com.example.johnson.billscanner.entities.DashboarEntity;
import com.example.johnson.billscanner.entities.FlowEntity;
import com.example.johnson.billscanner.interfaces.Constansts;
import com.example.johnson.billscanner.interfaces.Controller;
import com.example.johnson.billscanner.utils.Constants;
import com.google.gson.Gson;

public class TypeDialog extends BaseDialog {

    Controller controller;
    RelativeLayout rlPersonal, rlBusiness;
    FlowEntity dashboarEntity;

    @Override
    protected void initView(View view) {

        rlPersonal = view.findViewById(R.id.rl_personal);
        rlBusiness = view.findViewById(R.id.rl_business);
        setActionListener();

    }

    @Override
    protected int getLayout() {
        return R.layout.choosetype_layout;
    }


    public static TypeDialog newInstance(Controller controller, FlowEntity dashboarEntity) {

        TypeDialog typeDialog = new TypeDialog();
        typeDialog.controller = controller;
        typeDialog.dashboarEntity = dashboarEntity;
        return typeDialog;

    }

    /**
     * @param savedInstanceState
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

    }

    /**
     * Default method
     *
     * @param savedInstanceState
     * @return
     */
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        // the content
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(controller.getBaseActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }


    private void setActionListener() {
        rlBusiness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(controller.getBaseActivity(),TypeOfProcessActivity.class);
                Bundle bundle = new Bundle();

                dashboarEntity.setAccountType("2");
                bundle.putString(Constansts.ARG_FLOW,new Gson().toJson(dashboarEntity));
                intent.putExtras(bundle);
                controller.getBaseActivity().startActivity(intent);
            }
        });


        rlPersonal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(controller.getBaseActivity(),TypeOfProcessActivity.class);
                Bundle bundle = new Bundle();
                dashboarEntity.setAccountType("1");
                bundle.putString(Constansts.ARG_FLOW,new Gson().toJson(dashboarEntity));
                intent.putExtras(bundle);
                controller.getBaseActivity().startActivity(intent);

            }
        });
    }
}
