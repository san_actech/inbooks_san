package com.example.johnson.billscanner.entities;

public class ChooseActionEntity extends BaseEntity {

    String key;
    boolean isSelected;

    public ChooseActionEntity(String key, boolean isSelected) {
        this.key = key;
        this.isSelected = isSelected;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
