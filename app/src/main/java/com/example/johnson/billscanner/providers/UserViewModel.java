package com.example.johnson.billscanner.providers;

import androidx.lifecycle.ViewModel;

import com.example.johnson.billscanner.entities.UserModel;
import com.example.johnson.billscanner.listeners.DataListener;
import com.example.johnson.billscanner.utils.FButils;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class UserViewModel extends ViewModel {


    public void getUser(String id, final DataListener<UserModel> dataListener) {

        FirebaseFirestore.getInstance().collection(FButils.TB_NAMES.USER_INSTANCE).document(id).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {

                dataListener.onItemUpdated(documentSnapshot.getId(), documentSnapshot.toObject(UserModel.class));

            }
        });
    }


    public void addNewUser(UserModel userModel, final DataListener<UserModel> dataListener) {

        FirebaseFirestore.getInstance().collection(FButils.TB_NAMES.USER_INSTANCE).document(FirebaseAuth.getInstance().getCurrentUser().getUid()).set(userModel).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dataListener.onItemAdded(FirebaseAuth.getInstance().getCurrentUser().getUid(), null);
            }
        });
    }
}
