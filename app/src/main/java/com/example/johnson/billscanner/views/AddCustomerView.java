package com.example.johnson.billscanner.views;

import com.example.johnson.billscanner.interfaces.Controller;

public class AddCustomerView extends BaseView {


    public AddCustomerView(Controller controller) {
        super(controller);
    }

    @Override
    protected int getViewLayout() {
        return 0;
    }

    @Override
    protected void onCreate() {

    }

    @Override
    protected void setActionListeners() {

    }
}
