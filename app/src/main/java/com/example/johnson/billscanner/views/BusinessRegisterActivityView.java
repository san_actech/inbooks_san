package com.example.johnson.billscanner.views;

import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.johnson.billscanner.R;
import com.example.johnson.billscanner.activities.BusinessRegisterActivity;
import com.example.johnson.billscanner.databinding.BusinessRegisterActivityBinding;
import com.example.johnson.billscanner.entities.BusinessEntity;
import com.example.johnson.billscanner.interfaces.Controller;
import com.example.johnson.billscanner.utils.PermissionUtils;
import com.google.firebase.auth.FirebaseAuth;
import zendesk.belvedere.Belvedere;
import zendesk.belvedere.ImageStream;


public class BusinessRegisterActivityView extends BaseView {

    ImageView ivLogo;
    private ImageStream imageStream;
    private Button btnNext;
    private BusinessRegisterActivityBinding businessRegisterActivityBinding;


    public BusinessRegisterActivityView(Controller controller) {
        super(controller);
    }

    @Override
    protected int getViewLayout() {
        return R.layout.business_register_activity;
    }

    @Override
    protected void onCreate() {

        businessRegisterActivityBinding = DataBindingUtil.setContentView(controller.getBaseActivity(), R.layout.business_register_activity);

        Glide.with(controller.getBaseActivity())
                .load(R.drawable.image_capture)
                .into(businessRegisterActivityBinding.imgLogo);

    }

    @Override
    protected void setActionListeners() {

        businessRegisterActivityBinding.imgLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    if (PermissionUtils.getInstance(controller.getBaseActivity()).hasCameraPermission()) {

                        Belvedere.from(controller.getBaseActivity())
                                .document()
                                .open(controller.getBaseActivity());

                    } else {
                        PermissionUtils.getInstance(controller.getBaseActivity()).requestCameraPermission(controller.getBaseActivity());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        businessRegisterActivityBinding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validate()) {
                    BusinessEntity businessEntity = new BusinessEntity(businessRegisterActivityBinding.name.getText().toString(), businessRegisterActivityBinding.phone.getText().toString(),
                            businessRegisterActivityBinding.address.getText().toString(), businessRegisterActivityBinding.email.getText().toString(), businessRegisterActivityBinding.cin.getText().toString(),
                            businessRegisterActivityBinding.tan.getText().toString(), businessRegisterActivityBinding.pan.getText().toString(), businessRegisterActivityBinding.gst.getText().toString(), FirebaseAuth.getInstance().getCurrentUser().getUid());

                    ((BusinessRegisterActivity) controller.getBaseActivity()).storeBusiness(businessEntity);
                }
            }
        });
    }

    public void loadPhoto(Uri uri) {
        Glide.with(controller.getBaseActivity())
                .load(uri)
                .apply(RequestOptions.circleCropTransform())
                .placeholder(R.drawable.image_capture)
                .into(ivLogo);
    }


    private boolean validate() {
        if (TextUtils.isEmpty(businessRegisterActivityBinding.name.getText().toString().trim())) {
            businessRegisterActivityBinding.name.setError("Enter business name");
            return false;
        } else if (TextUtils.isEmpty(businessRegisterActivityBinding.phone.getText().toString())) {
            businessRegisterActivityBinding.phone.setError("Enter phone number");
            return false;
        } else if (TextUtils.isEmpty(businessRegisterActivityBinding.address.getText().toString())) {
            businessRegisterActivityBinding.address.setError("Enter address");
            return false;
        } else if (TextUtils.isEmpty(businessRegisterActivityBinding.email.getText().toString())) {
            businessRegisterActivityBinding.email.setError("Enter your email");
            return false;
        } else if (TextUtils.isEmpty(businessRegisterActivityBinding.cin.getText().toString())) {
            businessRegisterActivityBinding.cin.setError("Enter CIN number");
            return false;
        } else if (TextUtils.isEmpty(businessRegisterActivityBinding.tan.getText().toString())) {
            businessRegisterActivityBinding.tan.setError("Enter TAN number");
            return false;
        } else if (TextUtils.isEmpty(businessRegisterActivityBinding.pan.getText().toString())) {
            businessRegisterActivityBinding.pan.setError("Enter PAN number");
            return false;
        } else if (TextUtils.isEmpty(businessRegisterActivityBinding.gst.getText().toString())) {
            businessRegisterActivityBinding.gst.setError("Enter GST number");
            return false;
        }

        return true;
    }
}
