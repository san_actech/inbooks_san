package com.example.johnson.billscanner.viewholders;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.johnson.billscanner.R;
import com.example.johnson.billscanner.entities.ChooseActionEntity;
import com.example.johnson.billscanner.interfaces.Controller;
import com.example.johnson.billscanner.listeners.ChooseActionListener;

public class ChooseActionViewHolder extends BaseViewHolder<ChooseActionEntity> {

    Controller controller;
    View view;
    TextView name;
    ChooseActionListener chooseVendorsActionClickLIstener;
    RelativeLayout relativeLayout;
    ChooseActionEntity entity;

    public ChooseActionViewHolder(View itemView, Controller mController, ChooseActionListener chooseVendorsActionClickLIstener) {
        super(itemView);

        this.view = itemView;
        this.controller = mController;
        this.chooseVendorsActionClickLIstener = chooseVendorsActionClickLIstener;
        name = itemView.findViewById(R.id.itemTextView);
        relativeLayout = itemView.findViewById(R.id.rl_container);
        setActionListenre();
    }


    @Override
    public void bind(ChooseActionEntity entity) {
        this.entity = entity;

        name.setText(entity.getKey());

    }


    private void setActionListenre(){

        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chooseVendorsActionClickLIstener != null)
                    chooseVendorsActionClickLIstener.onItemClick(entity);
            }
        });

    }
}