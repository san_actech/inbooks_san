package com.example.johnson.billscanner.entities;

import java.util.ArrayList;
import java.util.List;

public class VendorsList extends BaseEntity {

    List<VendorEntity> vendorEntities = new ArrayList<>();


    public List<VendorEntity> getVendorEntities() {
        return vendorEntities;
    }

    public void setVendorEntities(List<VendorEntity> vendorEntities) {
        this.vendorEntities = vendorEntities;
    }
}
