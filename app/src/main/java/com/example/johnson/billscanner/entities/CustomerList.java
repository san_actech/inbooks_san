package com.example.johnson.billscanner.entities;

import java.util.ArrayList;
import java.util.List;

public class CustomerList extends BaseEntity {


    List<CustomerEntity> vendorEntities = new ArrayList<>();


    public List<CustomerEntity> getcustomerEntities() {
        return vendorEntities;
    }

    public void setCustomerEntities(List<CustomerEntity> vendorEntities) {
        this.vendorEntities = vendorEntities;
    }
}
