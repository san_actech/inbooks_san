package com.example.johnson.billscanner.activities;

import androidx.lifecycle.ViewModelProviders;

import com.example.johnson.billscanner.entities.BusinessList;
import com.example.johnson.billscanner.interfaces.Controller;
import com.example.johnson.billscanner.listeners.DataListener;
import com.example.johnson.billscanner.providers.BusinessViewModel;
import com.example.johnson.billscanner.views.BaseView;
import com.example.johnson.billscanner.views.BusinessListView;
import com.google.firebase.auth.FirebaseAuth;

public class BusinessListActivity extends BaseActivity {
    @Override
    protected BaseView getViewForController(Controller controller) {
        return new BusinessListView(controller);
    }



    public void getBusinessList(){

        BusinessViewModel businessViewModel = ViewModelProviders.of(this).get(BusinessViewModel.class);

        businessViewModel.getBusinessForUser(FirebaseAuth.getInstance().getCurrentUser().getUid(), new DataListener<BusinessList>() {
            @Override
            public void onItemAdded(String key, BusinessList data) {

            }

            @Override
            public void onItemUpdated(String key, BusinessList updatedData) {

                ((BusinessListView)view).setListInAdapter(updatedData);


            }

            @Override
            public void onItemDeleted(String deletedItemKey) {

            }

            @Override
            public void onFailed(int errorCode, String errorMsg) {

            }
        });
    }
}
