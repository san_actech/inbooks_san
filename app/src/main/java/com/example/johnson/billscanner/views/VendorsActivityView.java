package com.example.johnson.billscanner.views;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.view.View;

import com.example.johnson.billscanner.R;
import com.example.johnson.billscanner.activities.AddVendorsActivity;
import com.example.johnson.billscanner.activities.PickScanActivity;
import com.example.johnson.billscanner.activities.VendorsActivity;
import com.example.johnson.billscanner.adapters.BaseRecyclerAdapter;
import com.example.johnson.billscanner.databinding.VendorLayoutBinding;
import com.example.johnson.billscanner.delegate.ChooseVendorDelegate;
import com.example.johnson.billscanner.delegate.CustomerDelegate;
import com.example.johnson.billscanner.entities.BaseEntity;
import com.example.johnson.billscanner.entities.CustomerEntity;
import com.example.johnson.billscanner.entities.CustomerList;
import com.example.johnson.billscanner.entities.FlowEntity;
import com.example.johnson.billscanner.entities.VendorEntity;
import com.example.johnson.billscanner.entities.VendorsList;
import com.example.johnson.billscanner.interfaces.Constansts;
import com.example.johnson.billscanner.interfaces.Controller;
import com.example.johnson.billscanner.listeners.ChooseVendorsActionClickLIstener;
import com.example.johnson.billscanner.utils.Loader;
import com.google.gson.Gson;

public class VendorsActivityView extends BaseView implements ChooseVendorsActionClickLIstener {


    private VendorLayoutBinding binding;
    private BaseRecyclerAdapter mAdapter;

    public VendorsActivityView(Controller controller) {
        super(controller);
    }

    @Override
    protected int getViewLayout() {
        return R.layout.type_of_process_layout;
    }

    @Override
    protected void onCreate() {

        binding = DataBindingUtil.setContentView(controller.getBaseActivity(), R.layout.vendor_layout);


        switch (((VendorsActivity) controller.getBaseActivity()).getFlowEntity().getComponents()){
            case PURCHASE:
                binding.txtViewTitle.setText("VENDORS");
                binding.tvFabText.setText("Add New Vendors");
                break;
            case SALES:
                binding.txtViewTitle.setText("CUSTOMERS");
                binding.tvFabText.setText("Add New Customer");
                break;
        }



        Loader.showLoader(controller.getBaseActivity(), "Loading", "Please wait");
        ((VendorsActivity) controller.getBaseActivity()).getList();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((VendorsActivity) controller.getBaseActivity()).getList();
    }

    @Override
    protected void setActionListeners() {

        binding.fab.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View v) {

                                               Intent intent = new Intent(controller.getBaseActivity(), AddVendorsActivity.class);
                                               Bundle bundle = new Bundle();
                                               bundle.putString(Constansts.ARG_FLOW, new Gson().toJson(((VendorsActivity) controller.getBaseActivity()).getFlowEntity()));
                                               intent.putExtras(bundle);
                                               controller.getBaseActivity().startActivity(intent);
                                           }
                                       }
        );
    }

    public void setListInAdapter(VendorsList vendorsList) {
        try {
            LinearLayoutManager layoutManager = new LinearLayoutManager(controller.getBaseActivity());
            binding.rvType.setLayoutManager(layoutManager);
            mAdapter = new BaseRecyclerAdapter(vendorsList.getVendorEntities());
            mAdapter.addAdapterDelegates(new ChooseVendorDelegate(controller, this));
            binding.rvType.setAdapter(mAdapter);
            Loader.hideLoader();
        } catch (Exception e) {
            e.printStackTrace();
            Loader.hideLoader();
        }
    }

    public void setListInAdapter(CustomerList vendorsList) {
        try {
            LinearLayoutManager layoutManager = new LinearLayoutManager(controller.getBaseActivity());
            binding.rvType.setLayoutManager(layoutManager);
            mAdapter = new BaseRecyclerAdapter(vendorsList.getcustomerEntities());
            mAdapter.addAdapterDelegates(new CustomerDelegate(controller, this));
            binding.rvType.setAdapter(mAdapter);
            Loader.hideLoader();
        } catch (Exception e) {
            e.printStackTrace();
            Loader.hideLoader();
        }
    }

    @Override
    public void onItemClick(Object entity) {

        FlowEntity flowEntity = ((VendorsActivity) controller.getBaseActivity()).getFlowEntity();

        switch (((VendorsActivity) controller.getBaseActivity()).getFlowEntity().getComponents()){

            case SALES:
                CustomerEntity customerEntity = (CustomerEntity) entity;
                flowEntity.setVendorsId(customerEntity.getId());
                break;

            case PURCHASE:
                VendorEntity vendorEntity = (VendorEntity) entity;
                flowEntity.setVendorsId(vendorEntity.getVendorsId());
                break;
        }

        Intent intent = new Intent(controller.getBaseActivity(), PickScanActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(Constansts.ARG_FLOW, new Gson().toJson(flowEntity));
        intent.putExtras(bundle);
        getBaseActivity().startActivity(intent);
    }
}
