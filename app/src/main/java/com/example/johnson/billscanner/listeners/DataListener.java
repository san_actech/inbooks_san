package com.example.johnson.billscanner.listeners;

import com.example.johnson.billscanner.entities.BaseEntity;

public interface DataListener<T extends BaseEntity> {

    void onItemAdded(String key, T data);

    void onItemUpdated(String key, T updatedData);

    void onItemDeleted(String deletedItemKey);

    void onFailed(int errorCode, String errorMsg);

}
