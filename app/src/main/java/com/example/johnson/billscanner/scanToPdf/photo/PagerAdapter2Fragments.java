package com.example.johnson.billscanner.scanToPdf.photo;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;


import com.example.johnson.billscanner.scanToPdf.camera.CameraFragment;


public class PagerAdapter2Fragments extends FragmentStatePagerAdapter {


    private static final int TAB_COUNT = 1;
    private static final int TAB_CAMERA = 0;

    public PagerAdapter2Fragments(FragmentManager fm) {
        super(fm);
    }

    @Override
    public CameraFragment getItem(int position) {

        switch(position) {

            case TAB_CAMERA:
                ScanCameraFragment profileInfoFragment = new ScanCameraFragment();
                ScanCameraFragment.setConfig(ImagePickerActivity.getConfig());
                return profileInfoFragment;
        }

        return null;
    }

    @Override
    public int getCount() {
        return TAB_COUNT;
    }


    @Override
    public int getItemPosition(Object object) {
        return POSITION_UNCHANGED;
    }
}
