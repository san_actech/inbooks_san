package com.example.johnson.billscanner.views;

import android.content.Intent;

import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import com.example.johnson.billscanner.R;
import com.example.johnson.billscanner.activities.BusinessRegisterActivity;
import com.example.johnson.billscanner.activities.UserRegisterActivity;
import com.example.johnson.billscanner.databinding.FragmentSignupBinding;
import com.example.johnson.billscanner.entities.UserModel;
import com.example.johnson.billscanner.interfaces.Controller;

public class UserRegisterActivityView extends BaseView {


    Button btnNext;
    private FragmentSignupBinding fragmentSignupBinding;

    public UserRegisterActivityView(Controller controller) {
        super(controller);
    }

    @Override
    protected int getViewLayout() {
        return R.layout.fragment_signup;
    }

    @Override
    protected void onCreate() {

        fragmentSignupBinding = DataBindingUtil.setContentView(controller.getBaseActivity(), R.layout.fragment_signup);
    }

    @Override
    protected int getToolbarId() {
        return R.id.toolbar;
    }

    @Override
    protected void setActionListeners() {

        fragmentSignupBinding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validate()){
                    UserModel userModel = new UserModel(fragmentSignupBinding.firstName.getText().toString().trim(),
                            fragmentSignupBinding.lastName.getText().toString(), fragmentSignupBinding.phoneNumber.getText().toString(), fragmentSignupBinding.city.getText().toString(),
                            fragmentSignupBinding.country.getText().toString(), fragmentSignupBinding.mal.getText().toString());

                    ((UserRegisterActivity) controller.getBaseActivity()).storeToDb(userModel);

                }

            }
        });
    }


    @Override
    protected void onToolBarSetup(Toolbar toolBar) {
        try {
            ((UserRegisterActivity) controller).getSupportActionBar().setTitle("User Registration");
        } catch (Exception je) {
            je.printStackTrace();
        }
    }


    private boolean validate() {
        if (TextUtils.isEmpty(fragmentSignupBinding.firstName.getText().toString().trim())) {
            fragmentSignupBinding.firstName.setError("Enter first name");
            return false;
        } else if (TextUtils.isEmpty(fragmentSignupBinding.lastName.getText().toString())) {
            fragmentSignupBinding.lastName.setError("Enter last name");
            return false;
        } else if (TextUtils.isEmpty(fragmentSignupBinding.phoneNumber.getText().toString())) {
            fragmentSignupBinding.phoneNumber.setError("Enter phone number");
            return false;
        } else if (TextUtils.isEmpty(fragmentSignupBinding.city.getText().toString())) {
            fragmentSignupBinding.city.setError("Enter city name");
            return false;
        } else if (TextUtils.isEmpty(fragmentSignupBinding.country.getText().toString())) {
            fragmentSignupBinding.country.setError("Enter county name");
            return false;
        } else if (TextUtils.isEmpty(fragmentSignupBinding.mal.getText().toString())) {
            fragmentSignupBinding.mal.setError("Enter mail address");
            return false;
        }

        return true;
    }
}
