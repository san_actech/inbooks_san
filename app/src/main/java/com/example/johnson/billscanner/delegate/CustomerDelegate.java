package com.example.johnson.billscanner.delegate;

import android.view.View;

import androidx.annotation.NonNull;

import com.example.johnson.billscanner.R;
import com.example.johnson.billscanner.adapters.AdapterDelegate;
import com.example.johnson.billscanner.interfaces.Controller;
import com.example.johnson.billscanner.listeners.ChooseVendorsActionClickLIstener;
import com.example.johnson.billscanner.viewholders.CustomerViewholder;

import java.util.List;

public class CustomerDelegate implements AdapterDelegate<CustomerViewholder> {


    Controller controller;
    ChooseVendorsActionClickLIstener chooseVendorsActionClickLIstener;

    public CustomerDelegate(Controller controller, ChooseVendorsActionClickLIstener chooseVendorsActionClickLIstener) {
        this.controller = controller;
        this.chooseVendorsActionClickLIstener = chooseVendorsActionClickLIstener;

    }

    @Override
    public boolean isForViewType(@NonNull List<?> items, int position) {
        return true;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.choose_action_layout;
    }

    @NonNull
    @Override
    public CustomerViewholder getViewHolder(View itemView) {
        return new CustomerViewholder(itemView,controller, chooseVendorsActionClickLIstener);
    }
}