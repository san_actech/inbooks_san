package com.example.johnson.billscanner.entities;

public class BusinessEntity extends BaseEntity {

    String name,phone,address,email,cin,tan,pan,gst,userId,businessId;


    public BusinessEntity() {

    }

    public BusinessEntity(String name, String phone, String address, String email, String cin, String tan, String pan, String gst, String userId) {
        this.name = name;
        this.phone = phone;
        this.address = address;
        this.email = email;
        this.cin = cin;
        this.tan = tan;
        this.pan = pan;
        this.gst = gst;
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCin() {
        return cin;
    }

    public void setCin(String cin) {
        this.cin = cin;
    }

    public String getTan() {
        return tan;
    }

    public void setTan(String tan) {
        this.tan = tan;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getGst() {
        return gst;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }
}
