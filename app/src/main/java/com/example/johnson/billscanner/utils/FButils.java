package com.example.johnson.billscanner.utils;

import com.example.johnson.billscanner.interfaces.Constansts;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.UUID;

public class FButils {


    public class TB_NAMES {

        public final static String USER_INSTANCE = "USERS";
        public final static String BUSINESS_INSTANCE = "BUSINESS";
        public final static String CUSTOMER_INSTANCE = "CUSTOMER";
        public final static String VENDORS_INSTANCE = "VENDORS";
        public final static String BUSINESS_DATA = "BUSINESS_DATA";
    }


    public static String getDocumentid(String Instance) {
        DocumentReference ref = FirebaseFirestore.getInstance().collection(Instance).document();
        return ref.getId();

    }

    public static String getDocumentid(CollectionReference collectionReference) {
        DocumentReference ref = collectionReference.document();
        return ref.getId();

    }

    public static StorageReference storeReference(String businessId, String financialYear, String month, Constansts.StorageType storageType) {

        StorageReference storageReference = null;

        switch (storageType) {
            case IMAGE:
                storageReference = FirebaseStorage.getInstance().getReference()
                        .child(businessId + "/" + Constansts.PURCHSE + "/" + financialYear + "/" + month + "/" + "IMAGE" + "/" + UUID.randomUUID().toString() + ".jpg");
                break;
            case DOCUMENT:
                storageReference = FirebaseStorage.getInstance().getReference()
                        .child(businessId + "/" + Constansts.PURCHSE + "/" + financialYear + "/" + month + "/" + "DOCUMENT" + "/" + UUID.randomUUID().toString() + ".jpg");
                break;
        }
        return storageReference;
    }


    public static DocumentReference getDocumentReferenceForCompents(Components components, String businessID, String finalcialYear) {

        DocumentReference documentReference = null;

        switch (components) {
            case LOANS:
                documentReference = FirebaseFirestore.getInstance().collection(TB_NAMES.USER_INSTANCE).document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                        .collection(TB_NAMES.BUSINESS_DATA).document(businessID).collection(Constansts.LOANS).document(finalcialYear);

                break;
            case SALES:

                documentReference = FirebaseFirestore.getInstance().collection(TB_NAMES.USER_INSTANCE).document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                        .collection(TB_NAMES.BUSINESS_DATA).document(businessID).collection(Constansts.SALES).document(finalcialYear);

                break;
            case ASSETS:
                documentReference = FirebaseFirestore.getInstance().collection(TB_NAMES.USER_INSTANCE).document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                        .collection(TB_NAMES.BUSINESS_DATA).document(businessID).collection(Constansts.ASSESTS).document(finalcialYear);

                break;
            case INCOME:

                documentReference = FirebaseFirestore.getInstance().collection(TB_NAMES.USER_INSTANCE).document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                        .collection(TB_NAMES.BUSINESS_DATA).document(businessID).collection(Constansts.INCOME).document(finalcialYear);
                break;
            case PENDING:

                documentReference = FirebaseFirestore.getInstance().collection(TB_NAMES.USER_INSTANCE).document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                        .collection(TB_NAMES.BUSINESS_DATA).document(businessID).collection(Constansts.PENDING).document(finalcialYear);
                break;

            case EXPENSES:
                documentReference = FirebaseFirestore.getInstance().collection(TB_NAMES.USER_INSTANCE).document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                        .collection(TB_NAMES.BUSINESS_DATA).document(businessID).collection(Constansts.EXPENSE).document(finalcialYear);
                break;

            case PURCHASE:
                documentReference = FirebaseFirestore.getInstance().collection(TB_NAMES.USER_INSTANCE).document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                        .collection(TB_NAMES.BUSINESS_DATA).document(businessID).collection(Constansts.PURCHSE_TB).document(finalcialYear);
                break;
        }

        return documentReference;
    }

}
