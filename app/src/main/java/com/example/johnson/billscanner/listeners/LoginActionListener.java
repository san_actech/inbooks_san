package com.example.johnson.billscanner.listeners;

public interface LoginActionListener {

    void onGetCodeClick(String mobileNo);

    void onVerifyClick(String Code);

    void onResendClick();
}
