package com.example.johnson.billscanner.providers;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;

import com.example.johnson.billscanner.entities.BusinessEntity;
import com.example.johnson.billscanner.entities.CustomerEntity;
import com.example.johnson.billscanner.entities.VendorEntity;
import com.example.johnson.billscanner.entities.VendorsList;
import com.example.johnson.billscanner.listeners.DataListener;
import com.example.johnson.billscanner.utils.FButils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class VendorsViewModell extends ViewModel {

    public void getVendor(String userId, String id, final DataListener<VendorEntity> dataListener) {

        FirebaseFirestore.getInstance().collection(FButils.TB_NAMES.USER_INSTANCE).document(userId).collection(FButils.TB_NAMES.VENDORS_INSTANCE).document(id).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {

                dataListener.onItemUpdated(documentSnapshot.getId(), documentSnapshot.toObject(VendorEntity.class));

            }
        });
    }


    public void addNewVendor(String userId, VendorEntity vendorEntity, final DataListener<VendorEntity> dataListener) {

        FirebaseFirestore.getInstance().collection(FButils.TB_NAMES.USER_INSTANCE).document(userId).collection(FButils.TB_NAMES.VENDORS_INSTANCE).add(vendorEntity).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {

                dataListener.onItemAdded(documentReference.getId(), null);
            }
        });
    }


    public void getVendorList(String id, String businessId, final DataListener<VendorsList> dataListener) {

        final List<VendorEntity> vendorEntities = new ArrayList<>();

        FirebaseFirestore.getInstance().collection(FButils.TB_NAMES.USER_INSTANCE).document(id).collection(FButils.TB_NAMES.VENDORS_INSTANCE).whereEqualTo("businessId", businessId).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                if (task.isSuccessful() && !task.getResult().getDocuments().isEmpty()) {

                    for (DocumentSnapshot snapshot : task.getResult().getDocuments()) {

                        VendorEntity vendorEntity = snapshot.toObject(VendorEntity.class);
                        if (vendorEntity != null) {
                            vendorEntity.setVendorsId(snapshot.getId());
                            vendorEntities.add(vendorEntity);
                        }
                    }
                }

                if (vendorEntities.size() > 0) {
                    VendorsList vendorsList = new VendorsList();
                    vendorsList.setVendorEntities(vendorEntities);
                    dataListener.onItemUpdated("", vendorsList);
                } else {
                    dataListener.onItemUpdated("", null);
                }
            }
        });
    }
}
