package com.example.johnson.billscanner.activities;

import android.content.Intent;

import androidx.lifecycle.ViewModelProviders;

import com.example.johnson.billscanner.entities.UserModel;
import com.example.johnson.billscanner.interfaces.Constansts;
import com.example.johnson.billscanner.interfaces.Controller;
import com.example.johnson.billscanner.listeners.DataListener;
import com.example.johnson.billscanner.providers.UserViewModel;
import com.example.johnson.billscanner.utils.Loader;
import com.example.johnson.billscanner.utils.PreferenceUtil;
import com.example.johnson.billscanner.views.BaseView;
import com.example.johnson.billscanner.views.UserRegisterActivityView;

public class UserRegisterActivity extends BaseActivity {

    @Override
    protected BaseView getViewForController(Controller controller) {

        return new UserRegisterActivityView(controller);
    }

    @Override
    public boolean hasToolbar() {
        return true;
    }

    @Override
    public String getActionBarTitle() {
        return "User Registration";
    }


    public void storeToDb(UserModel userModel) {

        Loader.showLoader(this,"Loading","Please wait");

        UserViewModel userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);

        userViewModel.addNewUser(userModel, new DataListener<UserModel>() {
            @Override
            public void onItemAdded(String key, UserModel data) {

                PreferenceUtil.saveString(UserRegisterActivity.this, Constansts.USER_KEY,key);

                Loader.hideLoader();

                startActivity(new Intent(UserRegisterActivity.this, BusinessRegisterActivity.class));
            }

            @Override
            public void onItemUpdated(String key, UserModel updatedData) {

            }

            @Override
            public void onItemDeleted(String deletedItemKey) {

            }

            @Override
            public void onFailed(int errorCode, String errorMsg) {

            }
        });
    }

}
