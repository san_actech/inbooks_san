package com.example.johnson.billscanner.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProviders;

import com.example.johnson.billscanner.entities.BaseEntity;
import com.example.johnson.billscanner.entities.FlowEntity;
import com.example.johnson.billscanner.interfaces.Constansts;
import com.example.johnson.billscanner.interfaces.Controller;
import com.example.johnson.billscanner.listeners.DataListener;
import com.example.johnson.billscanner.providers.PurchaseViewModell;
import com.example.johnson.billscanner.utils.DocumentImageUtilModel;
import com.example.johnson.billscanner.utils.FButils;
import com.example.johnson.billscanner.utils.Loader;
import com.example.johnson.billscanner.utils.PreferenceUtil;
import com.example.johnson.billscanner.views.BaseView;
import com.example.johnson.billscanner.views.PickScanActiityView;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import zendesk.belvedere.Belvedere;
import zendesk.belvedere.Callback;
import zendesk.belvedere.MediaResult;


public class PickScanActivity extends BaseActivity {

    List<DocumentImageUtilModel> imageStreams = new ArrayList<>();
    private FlowEntity flowEntity;

    List<String> documentUrlList = new ArrayList<>();
    List<String> imageUrlList = new ArrayList<>();
    private String referenceNo;
    private String rmarks;

    @Override
    protected BaseView getViewForController(Controller controller) {

        Bundle bundle = getIntent().getExtras();
        flowEntity = new Gson().fromJson(bundle.getString(Constansts.ARG_FLOW, ""), FlowEntity.class);
        return new PickScanActiityView(controller);
    }

    protected void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Belvedere.from(this).getFilesFromActivityOnResult(requestCode, resultCode, data, new Callback<List<MediaResult>>() {
            @Override
            public void success(List<MediaResult> results) {

                if (results != null && !results.isEmpty()) {
                    MediaResult result = results.get(0);

                    if (result != null) {
                        File selectedImage = result.getFile();
                        String photoFilePath = selectedImage.getAbsolutePath();
                        if (photoFilePath != null && !photoFilePath.isEmpty()) {
                            Uri inputUri = Uri.fromFile(new File(photoFilePath));

                            DocumentImageUtilModel documentImageUtilModel = new DocumentImageUtilModel(requestCode, inputUri);
                            imageStreams.add(documentImageUtilModel);
                            ((PickScanActiityView) view).updateList(imageStreams);
                        } else {
                            Toast.makeText(PickScanActivity.this, "Unable to fetch selected Photo", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(PickScanActivity.this, "Unable to fetch selected Photo", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    public void uploadProductImage(final int position, final String refno, final String remarks) {
        referenceNo = refno;
        rmarks = remarks;
        Loader.showLoader(this, "Loading", "Please wait");
        InputStream inputStream = null;

        if (position < imageStreams.size()) {
            if (imageStreams.size() > 0) {
                try {
                    inputStream = getContentResolver().openInputStream(imageStreams.get(position).getImageStream());


                    StorageReference storageReference = FButils.storeReference(PreferenceUtil.getString(this, Constansts.BUSINESS_KEY, ""), flowEntity.getFinancialYear().trim(), flowEntity.getFinancialMonth(), Constansts.StorageType.IMAGE);
                    final UploadTask uploadTask = storageReference.putStream(inputStream);

                    uploadTask.addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            Log.d("ImageUpdloadFailed", exception.toString());
                        }
                    }).addOnCompleteListener(
                            new OnCompleteListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                                    if (imageStreams.get(position).getResultCode() == 1600)
                                        documentUrlList.add(task.getResult().getUploadSessionUri().toString());
                                    else
                                        imageUrlList.add(task.getResult().getUploadSessionUri().toString());

                                    Log.d("upload success", task.getResult().toString());

                                    uploadProductImage(position + 1, refno, remarks);

                                }
                            }
                    );

                    /*addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(final UploadTask.TaskSnapshot taskSnapshot) {

                            //product image URL.
                            taskSnapshot.getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {

                                }
                            });
                        }
                    });*/
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Loader.hideLoader();
                }
            }
        } else {
            Log.d("upload success", "uploadCompleted");
            uploadData();
        }

    }


    private void uploadData() {

        PurchaseViewModell purchaseViewModell = ViewModelProviders.of(this).get(PurchaseViewModell.class);

        flowEntity.setDocuments(documentUrlList);
        flowEntity.setImages(imageUrlList);
        flowEntity.setRefNo(referenceNo);
        flowEntity.setRemarks(rmarks);
        flowEntity.setBusinessId(PreferenceUtil.getString(this, Constansts.BUSINESS_KEY, ""));

        purchaseViewModell.uploadData(flowEntity, new DataListener() {
            @Override
            public void onItemAdded(String key, BaseEntity data) {
                Loader.hideLoader();
                startActivity(new Intent(PickScanActivity.this, MainActivity.class));
            }

            @Override
            public void onItemUpdated(String key, BaseEntity updatedData) {
                Loader.hideLoader();
                startActivity(new Intent(PickScanActivity.this, MainActivity.class));
            }

            @Override
            public void onItemDeleted(String deletedItemKey) {

            }

            @Override
            public void onFailed(int errorCode, String errorMsg) {

            }
        });
    }


    public FlowEntity getFlowEntity() {
        return flowEntity;
    }
}

