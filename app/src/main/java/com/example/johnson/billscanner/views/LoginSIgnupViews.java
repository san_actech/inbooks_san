package com.example.johnson.billscanner.views;

import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.view.MotionEvent;
import android.view.View;

import com.example.johnson.billscanner.R;
import com.example.johnson.billscanner.activities.LoginSingupActivity;
import com.example.johnson.billscanner.adapters.ViewPagerAdapter;
import com.example.johnson.billscanner.interfaces.Controller;
import com.example.johnson.billscanner.listeners.LoginActionListener;

public class LoginSIgnupViews extends BaseView implements LoginActionListener {

    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;
    private TabLayout tabLayout;

    public LoginSIgnupViews(Controller controller) {
        super(controller);
    }

    @Override
    protected int getViewLayout() {
        return R.layout.loginsignupactivity_view;
    }

    @Override
    protected void onCreate() {

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPagerAdapter = new ViewPagerAdapter((LoginSingupActivity)controller.getBaseActivity(),controller.getBaseActivity().getSupportFragmentManager());


        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });



        viewPager.setAdapter(viewPagerAdapter);
    }

    @Override
    protected void setActionListeners() {

    }

    @Override
    public void onGetCodeClick(String mobileNo) {


    }

    @Override
    public void onVerifyClick(String Code) {

    }

    @Override
    public void onResendClick() {

    }

    public void setPage(int position){
        viewPager.setCurrentItem(position,true);
    }



}
