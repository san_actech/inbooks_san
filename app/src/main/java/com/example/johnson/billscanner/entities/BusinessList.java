package com.example.johnson.billscanner.entities;

import java.util.ArrayList;
import java.util.List;

public class BusinessList extends BaseEntity {


    List<BusinessEntity> businessEntities = new ArrayList<>();

    public List<BusinessEntity> getBusinessEntities() {
        return businessEntities;
    }

    public void setBusinessEntities(List<BusinessEntity> businessEntities) {
        this.businessEntities = businessEntities;
    }
}
