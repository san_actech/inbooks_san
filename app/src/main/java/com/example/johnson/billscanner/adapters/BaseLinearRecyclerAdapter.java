package com.example.johnson.billscanner.adapters;

import com.example.johnson.billscanner.entities.BaseEntity;
import com.example.johnson.billscanner.viewholders.BaseViewHolder;

import java.util.List;

/**
 * Created on 01/02/2016.
 */
public abstract class BaseLinearRecyclerAdapter<VH extends BaseViewHolder> extends BaseRecyclerAdapter<VH> {

    protected BaseLinearRecyclerAdapter(List<?> entityList) {
        super(entityList);
    }

    /**
     * Call this method when data in {@code Adapter}
     * needs to be updated.
     * <br/> This method cannot be overridden. To add custom implementation,
     * override {@link BaseLinearRecyclerAdapter#willUpdateData()} and
     * {@link BaseLinearRecyclerAdapter#didUpdateData()}
     * @param list A collection of {@link BaseEntity} objects
     *
     * @see BaseLinearRecyclerAdapter#willUpdateData()
     * @see BaseLinearRecyclerAdapter#didUpdateData()
     */

    public final void updateData(List<? extends BaseEntity> list)
    {
        willUpdateData();

        int positionStart = 0;
        int size = 0;

        if (entityList != null && entityList.size() > 0  && list != null && list.size() > 0)
        {
            positionStart = entityList.size() + 1;
            size = list.size() - entityList.size();
        }

        entityList = list;
        if (positionStart >= size)
        {
            notifyDataSetChanged();

            return;
        }

        notifyItemRangeInserted(positionStart, size);

        didUpdateData();
    }

    @SuppressWarnings({"NoopMethodInAbstractClass", "WeakerAccess"})
    protected void willUpdateData() {

    }

    @SuppressWarnings({"NoopMethodInAbstractClass", "WeakerAccess"})
    protected void didUpdateData() {

    }
}
