package com.example.johnson.billscanner.utils;

import android.net.Uri;

public class DocumentImageUtilModel {

    int resultCode;

    Uri imageStream;


    public DocumentImageUtilModel(int resultCode, Uri imageStream) {
        this.resultCode = resultCode;
        this.imageStream = imageStream;
    }

    public int getResultCode() {
        return resultCode;
    }

    public Uri getImageStream() {
        return imageStream;
    }
}
