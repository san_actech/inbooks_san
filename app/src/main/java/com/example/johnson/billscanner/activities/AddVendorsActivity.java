package com.example.johnson.billscanner.activities;

import android.os.Bundle;

import com.example.johnson.billscanner.entities.FlowEntity;
import com.example.johnson.billscanner.interfaces.Constansts;
import com.example.johnson.billscanner.interfaces.Controller;
import com.example.johnson.billscanner.views.AddVendorsActivityView;
import com.example.johnson.billscanner.views.BaseView;
import com.google.gson.Gson;

public class AddVendorsActivity extends BaseActivity {

    private FlowEntity flowEntity;

    @Override
    protected BaseView getViewForController(Controller controller) {

        Bundle bundle = getIntent().getExtras();
        flowEntity = new Gson().fromJson(bundle.getString(Constansts.ARG_FLOW, ""), FlowEntity.class);
        return new AddVendorsActivityView(controller);
    }


    public FlowEntity getFlowEntity() {
        return flowEntity;
    }

    public void setFlowEntity(FlowEntity flowEntity) {
        this.flowEntity = flowEntity;
    }
}
