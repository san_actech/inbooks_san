package com.example.johnson.billscanner.listeners;

/**
 * Created by mohamedzakaria on 11/22/16.
 */

public interface GridClickListener {
    void onClick(int pos);
}
