package com.example.johnson.billscanner.entities;

public class VendorEntity extends BaseEntity {

    String name, address, gst, pan, email, phone, reference,businessId,vendorsId;

    public VendorEntity() {
    }

    public VendorEntity(String name, String address, String gst, String pan, String email, String phone, String reference, String businessId) {
        this.name = name;
        this.address = address;
        this.gst = gst;
        this.pan = pan;
        this.email = email;
        this.phone = phone;
        this.reference = reference;
        this.businessId = businessId;
    }


    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getGst() {
        return gst;
    }

    public String getPan() {
        return pan;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getReference() {
        return reference;
    }

    public String getBusinessId() {
        return businessId;
    }


    public String getVendorsId() {
        return vendorsId;
    }

    public void setVendorsId(String vendorsId) {
        this.vendorsId = vendorsId;
    }
}
