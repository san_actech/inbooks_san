package com.example.johnson.billscanner.viewholders;

import android.annotation.TargetApi;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.appcompat.widget.PopupMenu;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.johnson.billscanner.R;
import com.example.johnson.billscanner.entities.DashboarEntity;
import com.example.johnson.billscanner.interfaces.Controller;
import com.example.johnson.billscanner.listeners.DashboarItemClickListener;

import java.util.Random;


public class DashboardViewHolders extends BaseViewHolder<DashboarEntity> {
    private Controller mController;
    private DashboarItemClickListener mListener;
    TextView mTextView, tvCount;
    RelativeLayout mLinearLayout;
    CardView cardView;
    ImageView icon;
    ImageView menu;
    DashboarEntity entity;
    private Random mRandom = new Random();


    /*
     *Initialize layout variables
     * */
    public DashboardViewHolders(View itemView, Controller mController, DashboarItemClickListener mListener) {
        super(itemView);
        this.mController = mController;

        this.mListener = mListener;

        mTextView = (TextView) itemView.findViewById(R.id.tv);
        tvCount = (TextView) itemView.findViewById(R.id.tv_count);
        cardView = itemView.findViewById(R.id.card_view);
        icon = itemView.findViewById(R.id.iv_icon);
//        menu = itemView.findViewById(R.id.menu);
        mLinearLayout = itemView.findViewById(R.id.rl_container2);

        setActionListener();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void bind(DashboarEntity entity) {
        if (entity != null) {
            this.entity = entity;

            if (entity.getMenuItem().equals(mController.getBaseActivity().getString(R.string.purchase))) {

                Glide.with(mController.getBaseActivity())
                        .load(R.drawable.purchase)
                        .into(icon);

            } else if (entity.getMenuItem().equals(mController.getBaseActivity().getString(R.string.income))) {

                Glide.with(mController.getBaseActivity())
                        .load(R.drawable.income)
                        .into(icon);

            } else if (entity.getMenuItem().equals(mController.getBaseActivity().getString(R.string.loans))) {

                Glide.with(mController.getBaseActivity())
                        .load(R.drawable.loan)
                        .into(icon);
            } else if (entity.getMenuItem().equals(mController.getBaseActivity().getString(R.string.sales))) {

                Glide.with(mController.getBaseActivity())
                        .load(R.drawable.sales)
                        .into(icon);
            } else if (entity.getMenuItem().equals(mController.getBaseActivity().getString(R.string.expenses))) {

                Glide.with(mController.getBaseActivity())
                        .load(R.drawable.expenses)
                        .into(icon);
            } else if (entity.getMenuItem().equals(mController.getBaseActivity().getString(R.string.assets))) {

                Glide.with(mController.getBaseActivity())
                        .load(R.drawable.assets)
                        .into(icon);
            } else {

                Glide.with(mController.getBaseActivity())
                        .load(R.drawable.pending)
                        .into(icon);
            }


            int[] androidColors = mController.getBaseActivity().getResources().getIntArray(R.array.androidcolors);
            int randomAndroidColor = androidColors[new Random().nextInt(androidColors.length)];
            mLinearLayout.setBackgroundColor(randomAndroidColor);
            mTextView.setText(entity.getMenuItem());

        }

    }

    public void openOptionMenu(View v) {
        PopupMenu popup = new PopupMenu(v.getContext(), v);
        popup.getMenuInflater().inflate(R.menu.vendors_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Toast.makeText(mController.getBaseActivity(), "You selected the action : ", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
        popup.show();
    }

    private void setActionListener() {
        mLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("item Clicked", "Clicked");
                mListener.onItemClick(entity);
            }
        });

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(entity);
            }
        });
    }


}