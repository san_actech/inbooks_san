package com.example.johnson.billscanner.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.johnson.billscanner.R;

/**
 * Provides the user interface which indicates that an operation is in progress
 * e.g. an I/O operation on network/disk. Typically, displays a
 * {@link ProgressDialog} and provides methods to interact with it.
 */
@SuppressWarnings("WeakerAccess")
public final class Loader {

    /**
     * The progress dialog.
     */
    private static ProgressDialog progressDialog;

    private Loader() {
    }

    /**
     * Shows a {@link ProgressDialog} with the provided title and description.
     *
     * @param activity Activity context
     * @param message  Text to be shown as the dialog title
     * @param title    Text to be shown as the dialog message
     */
    public static void showLoader(Activity activity, CharSequence message, String title) {
        try {
            if (isLoaderShowing()) {
                return;
            }
            LayoutInflater inflater = LayoutInflater.from(activity);
            View progressDialogView = inflater.inflate(R.layout.view_progress_dialog, null);
            TextView pm = (TextView) progressDialogView.findViewById(R.id.txtTitle);

            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(message);
            pm.setText(message);

            if (!TextUtils.isEmpty(title)) {
                progressDialog.setTitle(title);
            }

            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.show();

            progressDialog.setContentView(progressDialogView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showLoader(Context context, CharSequence message, String title) {
        try {
            if (isLoaderShowing()) {
                return;
            }
            LayoutInflater inflater = LayoutInflater.from(context);
            View progressDialogView = inflater.inflate(R.layout.view_progress_dialog, null);
            TextView pm = (TextView) progressDialogView.findViewById(R.id.txtTitle);

            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(message);
            pm.setText(message);

            if (!TextUtils.isEmpty(title)) {
                progressDialog.setTitle(title);
            }

            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.show();

            progressDialog.setContentView(progressDialogView);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Hides the {@link ProgressDialog} if being displayed. Internally handles
     * possible exceptions.
     */

    public static void hideLoader() {
        try {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        } catch (Exception ignored) {

        }

    }

    /**
     * Returns true if Loader is showing
     *
     * @return {@code true}/{@code false}
     */
    public static boolean isLoaderShowing() {

        return (progressDialog != null) && progressDialog.isShowing();
    }
}
