package com.example.johnson.billscanner.delegate;

import androidx.annotation.NonNull;
import android.view.View;

import com.example.johnson.billscanner.R;
import com.example.johnson.billscanner.adapters.AdapterDelegate;
import com.example.johnson.billscanner.interfaces.Controller;
import com.example.johnson.billscanner.listeners.ChooseVendorsActionClickLIstener;
import com.example.johnson.billscanner.viewholders.ChooseVendorsViewHolder;

import java.util.List;

public class ChooseVendorDelegate implements AdapterDelegate<ChooseVendorsViewHolder> {


    Controller controller;
    ChooseVendorsActionClickLIstener chooseVendorsActionClickLIstener;

    public ChooseVendorDelegate(Controller controller, ChooseVendorsActionClickLIstener chooseVendorsActionClickLIstener) {
        this.controller = controller;
        this.chooseVendorsActionClickLIstener = chooseVendorsActionClickLIstener;

    }

    @Override
    public boolean isForViewType(@NonNull List<?> items, int position) {
        return true;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.choose_action_layout;
    }

    @NonNull
    @Override
    public ChooseVendorsViewHolder getViewHolder(View itemView) {
        return new ChooseVendorsViewHolder(itemView,controller, chooseVendorsActionClickLIstener);
    }
}
