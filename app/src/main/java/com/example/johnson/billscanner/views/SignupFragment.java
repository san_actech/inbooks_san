package com.example.johnson.billscanner.views;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.johnson.billscanner.R;
import com.example.johnson.billscanner.customfonts.InBookEditText;
import com.example.johnson.billscanner.customfonts.InBookTextView;
import com.example.johnson.billscanner.listeners.LoginActionListener;


public class SignupFragment extends Fragment {

    private LoginActionListener loginActionListener;
    private InBookEditText codeEditText;
    private Button btnVerify;
    private InBookTextView resend;

    public static SignupFragment newInstance(LoginActionListener loginActionListener) {
        SignupFragment fragment = new SignupFragment();
        fragment.loginActionListener = loginActionListener;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View verifyView =  inflater.inflate(R.layout.verify_layout, container, false);

        btnVerify = verifyView.findViewById(R.id.btn_verify);
        codeEditText = verifyView.findViewById(R.id.etd_code);
        resend = verifyView.findViewById(R.id.tv_resend);
        setActionListener();
        return verifyView;
    }

    private void setActionListener() {
        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(codeEditText.getText().toString())) {
                    codeEditText.setError("Enter your code");
                } else {
                    loginActionListener.onVerifyClick(codeEditText.getText().toString());
                }
            }
        });

        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginActionListener.onResendClick();
            }
        });
    }

}
