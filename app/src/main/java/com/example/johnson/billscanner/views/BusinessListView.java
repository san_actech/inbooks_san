package com.example.johnson.billscanner.views;

import android.content.Intent;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.johnson.billscanner.R;
import com.example.johnson.billscanner.activities.BusinessListActivity;
import com.example.johnson.billscanner.activities.BusinessRegisterActivity;
import com.example.johnson.billscanner.adapters.BaseRecyclerAdapter;
import com.example.johnson.billscanner.databinding.VendorLayoutBinding;
import com.example.johnson.billscanner.delegate.BusinessListDelegate;
import com.example.johnson.billscanner.entities.BusinessEntity;
import com.example.johnson.billscanner.entities.BusinessList;
import com.example.johnson.billscanner.interfaces.Constansts;
import com.example.johnson.billscanner.interfaces.Controller;
import com.example.johnson.billscanner.listeners.SelectBusinessListener;
import com.example.johnson.billscanner.utils.Loader;
import com.example.johnson.billscanner.utils.PreferenceUtil;

public class BusinessListView extends BaseView implements SelectBusinessListener {

    private VendorLayoutBinding vendorLayoutBinding;
    private BaseRecyclerAdapter mAdapter;

    public BusinessListView(Controller controller) {
        super(controller);
    }

    @Override
    protected int getViewLayout() {
        return R.layout.vendor_layout;
    }

    @Override
    protected void onCreate() {

        vendorLayoutBinding = DataBindingUtil.setContentView(controller.getBaseActivity(), R.layout.vendor_layout);

        vendorLayoutBinding.etdSearch.setHint(controller.getBaseActivity().getString(R.string.search_business));

        vendorLayoutBinding.tvFabText.setText(controller.getBaseActivity().getString(R.string.add_new_business));

        vendorLayoutBinding.txtViewTitle.setText(controller.getBaseActivity().getString(R.string.search_business));

        Loader.showLoader(controller.getBaseActivity(), "Loading", "Please wait");
        ((BusinessListActivity) controller.getBaseActivity()).getBusinessList();

    }

    @Override
    protected void setActionListeners() {

        vendorLayoutBinding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(controller.getBaseActivity(), BusinessRegisterActivity.class);
                controller.getBaseActivity().startActivity(intent);
            }
        });
    }


    public void setListInAdapter(BusinessList vendorsList) {
        try {
            LinearLayoutManager layoutManager = new LinearLayoutManager(controller.getBaseActivity());
            vendorLayoutBinding.rvType.setLayoutManager(layoutManager);
            mAdapter = new BaseRecyclerAdapter(vendorsList.getBusinessEntities());
            mAdapter.addAdapterDelegates(new BusinessListDelegate(controller, this));
            vendorLayoutBinding.rvType.setAdapter(mAdapter);
            Loader.hideLoader();
        } catch (Exception e) {
            Loader.hideLoader();
            e.printStackTrace();
        }
    }

    @Override
    public void OnBusinessClick(BusinessEntity businessEntity) {

        PreferenceUtil.saveString(controller.getBaseActivity(), Constansts.BUSINESS_KEY, businessEntity.getBusinessId());
        controller.getBaseActivity().finish();

    }
}
