package com.example.johnson.billscanner.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

import com.example.johnson.billscanner.R;
import com.example.johnson.billscanner.dialog.TypeDialog;
import com.example.johnson.billscanner.entities.BusinessEntity;
import com.example.johnson.billscanner.entities.DashboarEntity;
import com.example.johnson.billscanner.entities.FlowEntity;
import com.example.johnson.billscanner.interfaces.Constansts;
import com.example.johnson.billscanner.interfaces.Controller;
import com.example.johnson.billscanner.listeners.DataListener;
import com.example.johnson.billscanner.providers.BusinessViewModel;
import com.example.johnson.billscanner.scanToPdf.photo.ImagePickerActivity;
import com.example.johnson.billscanner.utils.Loader;
import com.example.johnson.billscanner.utils.PermissionUtils;
import com.example.johnson.billscanner.utils.PreferenceUtil;
import com.example.johnson.billscanner.views.BaseView;
import com.example.johnson.billscanner.views.DashboardViews;
import com.google.android.material.snackbar.Snackbar;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

public class MainActivity extends BaseActivity {

    public static final int INTENT_REQUEST_GET_IMAGES = 13;
    public static final int INTENT_REQUEST_GET_N_IMAGES = 14;

    BusinessEntity businessEntity;

    @Override
    protected BaseView getViewForController(Controller controller) {
        return new DashboardViews(controller);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        getBusiness();
    }


    private void getBusiness() {
        Loader.showLoader(this,"Loading","Please wait");

        String userId = PreferenceUtil.getString(this,Constansts.USER_KEY,"");
        String defaultBusinessID = PreferenceUtil.getString(this, Constansts.BUSINESS_KEY, "");
        BusinessViewModel businessViewModel = ViewModelProviders.of(this).get(BusinessViewModel.class);

        businessViewModel.getBusiness(userId,defaultBusinessID, new DataListener<BusinessEntity>() {
            @Override
            public void onItemAdded(String key, BusinessEntity data) {

            }

            @Override
            public void onItemUpdated(String key, BusinessEntity updatedData) {

                businessEntity = updatedData;
                ((DashboardViews)view).initUI(businessEntity);
            }

            @Override
            public void onItemDeleted(String deletedItemKey) {

            }

            @Override
            public void onFailed(int errorCode, String errorMsg) {

            }
        });
    }

    public BusinessEntity getBusinessEntity() {
        return businessEntity;
    }

    @Override
    public void setSnackbar(Snackbar snackBar) {

    }


    public void navigateToCa() {
        try {

            if (PermissionUtils.getInstance(this).hasCameraPermission()) {
                Intent intent = new Intent(getBaseActivity(), ImagePickerActivity.class);
                startActivityForResult(intent, INTENT_REQUEST_GET_IMAGES);
            } else {
                PermissionUtils.getInstance(this).requestCameraPermission(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showDialogType(FlowEntity dashboarEntity) {
        TypeDialog typeDialog = TypeDialog.newInstance(getBaseActivity(), dashboarEntity);
        ((BaseActivity) getBaseActivity()).showDialog(typeDialog);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        view.onActivityResult(requestCode, resultCode, data);
    }

    /*private boolean hasCameraPermission() {

        return ((ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) &&

                (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) &&

                (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED));
    }

    private void requestCameraPermission() {

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 300);
    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (PermissionUtils.getInstance(this).hasCameraPermission()) {
            navigateToCa();
        } else {
            return;
        }
    }

   /* @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(((DashboardViews)view).t.onOptionsItemSelected(item))
            return true;

        return super.onOptionsItemSelected(item);
    }*/

}
