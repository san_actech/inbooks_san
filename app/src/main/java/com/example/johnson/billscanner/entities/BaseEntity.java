package com.example.johnson.billscanner.entities;


import java.util.Date;

/**
 * An abstract class that is to be used as base for all the entities(POJO) in the project
 * , specially the entities that are used for web service response
 */

@SuppressWarnings({"ClassMayBeInterface", "EmptyClass"})
public abstract class BaseEntity {

    public static String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    protected long timeSince(Date date) {
        if(date == null) {
            return -1;
        }
        return System.currentTimeMillis() - date.getTime();
    }

    protected long timeLeft(Date date) {
        if(date == null) {
            return -1;
        }
        return date.getTime() - System.currentTimeMillis();
    }
}
