package com.example.johnson.billscanner.views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;

import androidx.core.content.FileProvider;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;

import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.johnson.billscanner.activities.BusinessListActivity;
import com.example.johnson.billscanner.activities.MainActivity;
import com.example.johnson.billscanner.R;
import com.example.johnson.billscanner.adapters.BaseRecyclerAdapter;
import com.example.johnson.billscanner.databinding.ActivityMainBinding;
import com.example.johnson.billscanner.entities.BusinessEntity;
import com.example.johnson.billscanner.entities.DashboarEntity;
import com.example.johnson.billscanner.entities.FlowEntity;
import com.example.johnson.billscanner.interfaces.Controller;
import com.example.johnson.billscanner.listeners.DashboarItemClickListener;
import com.example.johnson.billscanner.scanToPdf.photo.ImagePickerActivity;
import com.example.johnson.billscanner.scanToPdf.photo.model.Image;
import com.example.johnson.billscanner.scanToPdf.photo.utils.ImageInternalFetcher;
import com.example.johnson.billscanner.utils.Components;
import com.example.johnson.billscanner.utils.Loader;
import com.google.android.material.navigation.NavigationView;
import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;

public class DashboardViews extends BaseView implements DashboarItemClickListener {

    RecyclerView recyclerView;
    BaseRecyclerAdapter mAdapter;
    TextView month, year;
    LinearLayout llmonth, llyear;

    private DrawerLayout dl;
    public ActionBarDrawerToggle t;
    private NavigationView nv;
    Controller controller;


    public static DashboarEntity dashboarEntity;
    private ActivityMainBinding activityMainBinding;

    public DashboardViews(Controller controller) {
        super(controller);
        this.controller = controller;
    }

    @Override
    protected int getViewLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate() {

        activityMainBinding = DataBindingUtil.setContentView(controller.getBaseActivity(), R.layout.activity_main);


        new DrawerBuilder().withActivity(controller.getBaseActivity()).build();


        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(controller.getBaseActivity())
                .withHeaderBackground(R.drawable.usrusr)
                .addProfiles(
                        new ProfileDrawerItem().withName("John").withEmail("John@gmail.com").withIcon(controller.getBaseActivity().getResources().getDrawable(R.drawable.usrusr))
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        return false;
                    }
                })
                .build();

        PrimaryDrawerItem item1 = new PrimaryDrawerItem().withIdentifier(1).withName(R.string.drawer_item_home);
        SecondaryDrawerItem item2 = new SecondaryDrawerItem().withIdentifier(2).withName(R.string.drawer_item_settings);

//create the drawer and remember the `Drawer` result object
        Drawer result = new DrawerBuilder()
                .withActivity(controller.getBaseActivity())
                .withToolbar(activityMainBinding.toolbar)
                .withAccountHeader(headerResult)
                .addDrawerItems(
                        item1,
                        new DividerDrawerItem(),
                        new SecondaryDrawerItem().withName(R.string.drawer_item_settings)
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        // do something with the clicked item :D
                        return true;
                    }
                })
                .build();

        month = findViewById(R.id.tv_month);
        year = findViewById(R.id.tv_year);

        llyear = findViewById(R.id.ll_year);
        llmonth = findViewById(R.id.ll_month);

        SimpleDateFormat month_date = new SimpleDateFormat("MMM");
        SimpleDateFormat yearDate = new SimpleDateFormat("yyyy");
        String month_name = month_date.format(Calendar.getInstance().getTime());
        String year_name = yearDate.format(Calendar.getInstance().getTime());


        int year = Calendar.getInstance().get(Calendar.YEAR);



        String selected = String.valueOf(year);
        String next = String.valueOf(year + 1);
        activityMainBinding.tvMonth.setText(month_name);
        activityMainBinding.tvYear.setText(String.valueOf(selected + " " + "TO" + " " + next));

    }


    NavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener = new NavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            int id = item.getItemId();
            switch (id) {
                case R.id.account:
                    Toast.makeText(controller.getBaseActivity(), "My Account", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.settings:
                    Toast.makeText(controller.getBaseActivity(), "Settings", Toast.LENGTH_SHORT).show();
                    break;
                default:
                    return true;
            }


            return true;

        }
    };


    public void initUI(BusinessEntity businessEntity) {

/*        Toolbar toolbar = activityMainBinding.toolbar;

//        toolbar.setTitle(businessEntity.getName());
        controller.getBaseActivity().setSupportActionBar(toolbar);

        controller.getBaseActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        activityMainBinding.txtViewTitle.setText(businessEntity.getName());


//        dl = activityMainBinding.activityMain;
//        t = new ActionBarDrawerToggle(controller.getBaseActivity(), dl, R.string.Open, R.string.Close);
//
//        dl.addDrawerListener(t);
//        t.syncState();
//
//        activityMainBinding.nv.setNavigationItemSelectedListener(onNavigationItemSelectedListener);

        Loader.hideLoader();

    }

    @Override
    protected void setActionListeners() {

        activityMainBinding.llYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCalendar();
            }
        });

        activityMainBinding.llMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCalendar();
            }
        });

        activityMainBinding.cvPurchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FlowEntity entity = new FlowEntity();
                entity.setComponents(Components.PURCHASE);
                entity.setFinancialMonth(activityMainBinding.tvMonth.getText().toString().trim());
                entity.setFinancialYear(activityMainBinding.tvYear.getText().toString().trim());
                ((MainActivity) controller).showDialogType(entity);

//                DashboarEntity dashboarEntity = new DashboarEntity(controller.getBaseActivity().getString(R.string.purchase), Components.PURCHASE);
//                DashboardViews.dashboarEntity = dashboarEntity;
            }
        });

        activityMainBinding.cvSales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                DashboarEntity dashboarEntity = new DashboarEntity(controller.getBaseActivity().getString(R.string.sales), Components.SALES);
//                DashboardViews.dashboarEntity = dashboarEntity;
//                ((MainActivity) controller).showDialogType(dashboarEntity);

                FlowEntity entity = new FlowEntity();
                entity.setComponents(Components.SALES);
                entity.setFinancialMonth(activityMainBinding.tvMonth.getText().toString().trim());
                entity.setFinancialYear(activityMainBinding.tvYear.getText().toString().trim());
                ((MainActivity) controller).showDialogType(entity);
            }
        });

        activityMainBinding.cvIncome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                DashboarEntity dashboarEntity = new DashboarEntity(controller.getBaseActivity().getString(R.string.income), Components.INCOME);
//                DashboardViews.dashboarEntity = dashboarEntity;
//                ((MainActivity) controller).showDialogType(dashboarEntity);

                FlowEntity entity = new FlowEntity();
                entity.setComponents(Components.INCOME);
                entity.setFinancialMonth(activityMainBinding.tvMonth.getText().toString().trim());
                entity.setFinancialYear(activityMainBinding.tvYear.getText().toString().trim());
                ((MainActivity) controller).showDialogType(entity);
            }
        });

        activityMainBinding.cvAsset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                DashboarEntity dashboarEntity = new DashboarEntity(controller.getBaseActivity().getString(R.string.assets), Components.ASSETS);
//                DashboardViews.dashboarEntity = dashboarEntity;
//                ((MainActivity) controller).showDialogType(dashboarEntity);

                FlowEntity entity = new FlowEntity();
                entity.setComponents(Components.ASSETS);
                entity.setFinancialMonth(activityMainBinding.tvMonth.getText().toString().trim());
                entity.setFinancialYear(activityMainBinding.tvYear.getText().toString().trim());
                ((MainActivity) controller).showDialogType(entity);
            }
        });


        activityMainBinding.cvExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                DashboarEntity dashboarEntity = new DashboarEntity(controller.getBaseActivity().getString(R.string.expenses), Components.EXPENSES);
//                DashboardViews.dashboarEntity = dashboarEntity;
//                ((MainActivity) controller).showDialogType(dashboarEntity);

                FlowEntity entity = new FlowEntity();
                entity.setComponents(Components.EXPENSES);
                entity.setFinancialMonth(activityMainBinding.tvMonth.getText().toString().trim());
                entity.setFinancialYear(activityMainBinding.tvYear.getText().toString().trim());
                ((MainActivity) controller).showDialogType(entity);
            }
        });

        activityMainBinding.cvLoans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                DashboarEntity dashboarEntity = new DashboarEntity(controller.getBaseActivity().getString(R.string.loans), Components.LOANS);
//                DashboardViews.dashboarEntity = dashboarEntity;
//                ((MainActivity) controller).showDialogType(dashboarEntity);

                FlowEntity entity = new FlowEntity();
                entity.setComponents(Components.LOANS);
                entity.setFinancialMonth(activityMainBinding.tvMonth.getText().toString().trim());
                entity.setFinancialYear(activityMainBinding.tvYear.getText().toString().trim());
                ((MainActivity) controller).showDialogType(entity);
            }
        });

        activityMainBinding.cvPending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                DashboarEntity dashboarEntity = new DashboarEntity(controller.getBaseActivity().getString(R.string.pending_bills), Components.PENDING);
//                DashboardViews.dashboarEntity = dashboarEntity;
//                ((MainActivity) controller).showDialogType(dashboarEntity);

                FlowEntity entity = new FlowEntity();
                entity.setComponents(Components.PENDING);
                entity.setFinancialMonth(activityMainBinding.tvMonth.getText().toString().trim());
                entity.setFinancialYear(activityMainBinding.tvYear.getText().toString().trim());
                ((MainActivity) controller).showDialogType(entity);
            }
        });

        activityMainBinding.txtViewTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.getBaseActivity().startActivity(new Intent(controller.getBaseActivity(), BusinessListActivity.class));
            }
        });
    }


    private void openCalendar() {


        MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(controller.getBaseActivity(), new MonthPickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(int selectedMonth, int selectedYear) {

            }
        }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH));


        builder.setActivatedMonth(Calendar.MAY)
                .setMinYear(1990)
                .setActivatedYear(2019)
                .setMaxYear(2060)
                .setMinMonth(Calendar.JANUARY)
                .setTitle("Select Financial month")
                .setMonthRange(Calendar.JANUARY, Calendar.DECEMBER)
                .setOnMonthChangedListener(new MonthPickerDialog.OnMonthChangedListener() {
                    @Override
                    public void onMonthChanged(int selectedMonth) {

                        Calendar c = Calendar.getInstance();
                        c.set(Calendar.MONTH, selectedMonth);

                        SimpleDateFormat month_date = new SimpleDateFormat("MMM");
                        String month_name = month_date.format(c.getTime());
                        activityMainBinding.tvMonth.setText(month_name);
                    }
                })
                .setOnYearChangedListener(new MonthPickerDialog.OnYearChangedListener() {
                    @Override
                    public void onYearChanged(int selectedYear) {

                        String selected = String.valueOf(selectedYear);
                        String next = String.valueOf(selectedYear + 1);
                        activityMainBinding.tvYear.setText(String.valueOf(selected + " " + "TO" + " " + next));
                    }
                })
                .build()
                .show();


    }

    @Override
    public void onItemClick(DashboarEntity dashboarEntity) {

        DashboardViews.dashboarEntity = dashboarEntity;


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);

            if (resultCode == Activity.RESULT_OK) {

                if (requestCode == MainActivity.INTENT_REQUEST_GET_IMAGES || requestCode == MainActivity.INTENT_REQUEST_GET_N_IMAGES) {
                    Parcelable[] parcelableUris = data.getParcelableArrayExtra(ImagePickerActivity.EXTRA_IMAGE_URIS);
                    int[] parcelableOrientations = data.getIntArrayExtra((ImagePickerActivity.EXTRA_IMAGE_ORIENTATIONS));
                    if (parcelableUris == null) {
                        return;
                    }
                    Uri[] uris = new Uri[parcelableUris.length];
                    int[] orientations = new int[parcelableUris.length];
                    System.arraycopy(parcelableUris, 0, uris, 0, parcelableUris.length);
                    System.arraycopy(parcelableOrientations, 0, orientations, 0, parcelableOrientations.length);
                    if (uris != null) {
                        HashSet<Image> mMediaImages = new HashSet<Image>();
                        for (int i = 0; i < orientations.length; i++) {
                            mMediaImages.add(new Image(uris[i], orientations[i]));
                        }
                        showMedia(mMediaImages);
                    }
                }
            }
        } catch (Exception je) {
            je.printStackTrace();
        }
    }


    private void showMedia(HashSet<Image> mMediaImages) {
        try {

            Iterator<Image> iterator = mMediaImages.iterator();
            ImageInternalFetcher imageFetcher = new ImageInternalFetcher(controller.getBaseActivity(), 500);
            String directoryPath = Environment.getExternalStorageDirectory().getAbsolutePath();
            Date date = new Date();
            final String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(date);
            /*File myPDF = new File("Arinspect" + "/" + timeStamp + ".pdf");*/

            // point an output stream to our created document
            OutputStream output = null;
            try {

                output = new FileOutputStream(getDownloadDirectory("In + timeStamp + PDF + .pdf"));
//                output = new FileOutputStream(directoryPath + "/in-book" + "/In" + timeStamp + "PDF" + ".pdf");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            // create a document with difference page sizes depending on orientation
            Document document = new Document(PageSize.A4, 50, 50, 50, 50);


            String filename = "In" + timeStamp + "PDF" + ".pdf";
            File file = new File(directoryPath + "/In-book/" + filename);

            Uri fileURI = FileProvider.getUriForFile(getBaseActivity().getBaseContext(),
                    getBaseActivity().getBaseContext().getPackageName() + ".provider", file);

            openFile("application/pdf", file.getPath(), getBaseActivity());


            /*try {
                PdfWriter.getInstance(document, output);
            } catch (DocumentException e) {
                e.printStackTrace();
            }

            long startTime, estimatedTime;
            document.open();

            while (iterator.hasNext()) {
                Image image = iterator.next();
                if (!image.mUri.toString().contains("content://")) {
                    image.mUri = Uri.fromFile(new File(image.mUri.toString()));
                }
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getBaseActivity().getContentResolver(), image.mUri);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();

                    startTime = System.currentTimeMillis();

                    // changed from png to jpeg, lowered processing time greatly
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);

                    estimatedTime = System.currentTimeMillis() - startTime;

                    Log.e(TAG, "compressed image into stream: " + estimatedTime);

                    byte[] byteArray = stream.toByteArray();

                    // instantiate itext image
                    com.itextpdf.text.Image img = com.itextpdf.text.Image.getInstance(byteArray);

                    img.scaleToFit(PageSize.A4);
                    img.setAbsolutePosition(
                            (PageSize.A4.getWidth() - img.getScaledWidth()) / 2,
                            (PageSize.A4.getHeight() - img.getScaledHeight()) / 2
                    );

                    document.add(img);
                    document.newPage();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (document != null) {
                document.close();
            }*/

//            addMedia(fileURI, AppConstants.PDF, filename, file.getPath());
        } catch (Exception je) {
            je.printStackTrace();
        }
    }

    public static File getDownloadDirectory(String filenName) {

        File arinDirectory = new File(Environment.getExternalStorageDirectory() + "/In-book");

        if (!arinDirectory.exists()) {
            arinDirectory.mkdirs();
        }

        File outputFile = new File(arinDirectory, filenName);
        return outputFile;
    }


    public void openFile(String mimeType, String fileName, Context context) {

        //todo : add changes in ReferenceMaterialActivity for video
        File file = new File(fileName);

        Uri fileURI = FileProvider.getUriForFile(context, context.getPackageName() + ".provider", file);

        Intent newIntent = new Intent(Intent.ACTION_VIEW);
        newIntent.setDataAndType(fileURI, mimeType);
        newIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

        try {
            context.startActivity(newIntent);
        } catch (Exception e) {
            Toast.makeText(context, "Application Not Found", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }
}
