package com.example.johnson.billscanner.entities;

import com.example.johnson.billscanner.utils.Components;

import java.util.ArrayList;
import java.util.List;

public class DashboarEntity extends BaseEntity {

    String menuItem;
    Components components;

    public String getMenuItem() {
        return menuItem;
    }

    public void setMenuItem(String menuItem) {
        this.menuItem = menuItem;
    }

    public DashboarEntity(String menuItem, Components components) {
        this.menuItem = menuItem;
        this.components = components;
    }
}
