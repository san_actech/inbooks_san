package com.example.johnson.billscanner.activities;

import android.os.Bundle;
import android.view.View;

import androidx.lifecycle.ViewModelProviders;

import com.example.johnson.billscanner.entities.CustomerEntity;
import com.example.johnson.billscanner.entities.CustomerList;
import com.example.johnson.billscanner.entities.FlowEntity;
import com.example.johnson.billscanner.entities.VendorsList;
import com.example.johnson.billscanner.interfaces.Constansts;
import com.example.johnson.billscanner.interfaces.Controller;
import com.example.johnson.billscanner.listeners.DataListener;
import com.example.johnson.billscanner.providers.CustomerViewModel;
import com.example.johnson.billscanner.providers.VendorsViewModell;
import com.example.johnson.billscanner.utils.PreferenceUtil;
import com.example.johnson.billscanner.views.BaseView;
import com.example.johnson.billscanner.views.VendorsActivityView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;

public class VendorsActivity extends BaseActivity {

    private FlowEntity flowEntity;

    @Override
    protected BaseView getViewForController(Controller controller) {

        Bundle bundle = getIntent().getExtras();
        flowEntity = new Gson().fromJson(bundle.getString(Constansts.ARG_FLOW, ""), FlowEntity.class);
        return new VendorsActivityView(controller);
    }


    public void getList() {

        switch (flowEntity.getComponents()) {
            case LOANS:
                break;
            case SALES:
                CustomerViewModel customerViewModel = ViewModelProviders.of(this).get(CustomerViewModel.class);

                customerViewModel.getCustomerList(FirebaseAuth.getInstance().getCurrentUser().getUid(), PreferenceUtil.getString(this, Constansts.BUSINESS_KEY, ""), new DataListener<CustomerList>() {
                    @Override
                    public void onItemAdded(String key, CustomerList data) {
                        ((VendorsActivityView) view).setListInAdapter(data);
                    }

                    @Override
                    public void onItemUpdated(String key, CustomerList updatedData) {
                        ((VendorsActivityView) view).setListInAdapter(updatedData);
                    }

                    @Override
                    public void onItemDeleted(String deletedItemKey) {

                    }

                    @Override
                    public void onFailed(int errorCode, String errorMsg) {

                    }
                });

                break;

            case ASSETS:
                break;

            case INCOME:
                break;

            case PENDING:
                break;

            case EXPENSES:
                break;

            case PURCHASE:
                VendorsViewModell vendorsViewModell = ViewModelProviders.of(this).get(VendorsViewModell.class);

                vendorsViewModell.getVendorList(FirebaseAuth.getInstance().getCurrentUser().getUid(), PreferenceUtil.getString(this, Constansts.BUSINESS_KEY, ""), new DataListener<VendorsList>() {
                    @Override
                    public void onItemAdded(String key, VendorsList data) {

                    }

                    @Override
                    public void onItemUpdated(String key, VendorsList updatedData) {

                        ((VendorsActivityView) view).setListInAdapter(updatedData);

                    }

                    @Override
                    public void onItemDeleted(String deletedItemKey) {

                    }

                    @Override
                    public void onFailed(int errorCode, String errorMsg) {

                    }
                });
                break;
        }


    }

    public FlowEntity getFlowEntity() {
        return flowEntity;
    }
}
