package com.example.johnson.billscanner.viewholders;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.johnson.billscanner.R;
import com.example.johnson.billscanner.entities.BusinessEntity;
import com.example.johnson.billscanner.interfaces.Controller;
import com.example.johnson.billscanner.listeners.ChooseVendorsActionClickLIstener;
import com.example.johnson.billscanner.listeners.SelectBusinessListener;

public class BusinessListViewHolder extends BaseViewHolder<BusinessEntity> {

    Controller controller;
    View view;
    TextView name;
    SelectBusinessListener chooseVendorsActionClickLIstener;
    RelativeLayout relativeLayout;
    BusinessEntity entity;

    public BusinessListViewHolder(View itemView, Controller mController, SelectBusinessListener chooseVendorsActionClickLIstener) {
        super(itemView);

        this.view = itemView;
        this.controller = mController;
        this.chooseVendorsActionClickLIstener = chooseVendorsActionClickLIstener;
        name = itemView.findViewById(R.id.itemTextView);
        relativeLayout = itemView.findViewById(R.id.rl_container);
        setActionListenre();
    }


    @Override
    public void bind(BusinessEntity entity) {
        this.entity = entity;

        name.setText(entity.getName());

    }



    private void setActionListenre(){

        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chooseVendorsActionClickLIstener != null)
                    chooseVendorsActionClickLIstener.OnBusinessClick(entity);
            }
        });

    }
}