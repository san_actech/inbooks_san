package com.example.johnson.billscanner.activities;

import android.view.MenuItem;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.johnson.billscanner.R;
import com.example.johnson.billscanner.databinding.ScanlistActivityBinding;

import com.mzelzoghbi.zgallery.Constants;
import com.mzelzoghbi.zgallery.ZGallery;
import com.mzelzoghbi.zgallery.activities.BaseActivity;
import com.mzelzoghbi.zgallery.adapters.GridImagesAdapter;
import com.mzelzoghbi.zgallery.adapters.listeners.GridClickListener;
import com.mzelzoghbi.zgallery.entities.ZColor;

/**
 * Created by john on 12/05/2019.
 */
public final class ZGridActivity extends BaseActivity implements GridClickListener {
    private RecyclerView mRecyclerView;
    private GridImagesAdapter adapter;

    private int imgPlaceHolderResId;
    private int spanCount = 2;
    private ScanlistActivityBinding scanlistActivityBinding;

    @Override
    protected int getResourceLayoutId() {
        return R.layout.scanlist_activity;
    }

    @Override
    protected void afterInflation() {

        scanlistActivityBinding = DataBindingUtil.setContentView(this,R.layout.scanlist_activity);

        // get extra values
        imgPlaceHolderResId = getIntent().getIntExtra(Constants.IntentPassingParams.IMG_PLACEHOLDER, -1);
        spanCount = getIntent().getIntExtra(Constants.IntentPassingParams.COUNT, 2);

        adapter = new GridImagesAdapter(this, imageURLs, imgPlaceHolderResId);
        scanlistActivityBinding.recyclerView.setLayoutManager(new GridLayoutManager(this, spanCount));
        scanlistActivityBinding.recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(int pos) {
        ZGallery.with(this, imageURLs)
                .setToolbarTitleColor(ZColor.WHITE)
                .setToolbarColorResId(toolbarColorResId)
                .setSelectedImgPosition(pos)
                .setTitle(mToolbar.getTitle().toString())
                .show();
    }
}
