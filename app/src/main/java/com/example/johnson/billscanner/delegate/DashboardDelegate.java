package com.example.johnson.billscanner.delegate;

import androidx.annotation.NonNull;
import android.view.View;

import com.example.johnson.billscanner.R;
import com.example.johnson.billscanner.adapters.AdapterDelegate;
import com.example.johnson.billscanner.interfaces.Controller;
import com.example.johnson.billscanner.listeners.DashboarItemClickListener;
import com.example.johnson.billscanner.viewholders.DashboardViewHolders;

import java.util.List;

public class DashboardDelegate implements AdapterDelegate<DashboardViewHolders> {

    private Controller controller;
    DashboarItemClickListener dashboarItemClickListener;
//
    public DashboardDelegate(Controller controller, DashboarItemClickListener dashboarItemClickListener) {
        this.controller = controller;
        this.dashboarItemClickListener = dashboarItemClickListener;
    }

    @Override
    public boolean isForViewType (@NonNull List<?> items, int position) {

        return true;
    }

    @Override
    public int getLayoutRes () {

        return R.layout.dashboard_menu_item;
    }

    @NonNull
    @Override
    public DashboardViewHolders getViewHolder (View itemView) {

        return new DashboardViewHolders (itemView,controller,dashboarItemClickListener);
    }
}