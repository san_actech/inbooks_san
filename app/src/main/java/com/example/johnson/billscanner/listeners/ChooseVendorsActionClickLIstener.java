package com.example.johnson.billscanner.listeners;

import com.example.johnson.billscanner.entities.BaseEntity;
import com.example.johnson.billscanner.entities.ChooseActionEntity;
import com.example.johnson.billscanner.entities.VendorEntity;

public interface ChooseVendorsActionClickLIstener<T> {

    void onItemClick(T entity);
}
