package com.example.johnson.billscanner.adapters;



import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.johnson.billscanner.listeners.LoginActionListener;
import com.example.johnson.billscanner.views.LoginFragment;
import com.example.johnson.billscanner.views.SignupFragment;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private LoginActionListener loginActionListener;

    public ViewPagerAdapter(LoginActionListener loginActionListener,FragmentManager fm) {
        super(fm);
        this.loginActionListener = loginActionListener;
    }


    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0)
        {
            fragment = LoginFragment.newInstance(loginActionListener);
        }
        else if (position == 1)
        {
            fragment =SignupFragment.newInstance(loginActionListener);
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        return title;
    }
}
