package com.example.johnson.billscanner.dialog;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import androidx.databinding.DataBindingUtil;

import com.example.johnson.billscanner.R;
import com.example.johnson.billscanner.base.BaseDialog;
import com.example.johnson.billscanner.databinding.RemarksLayoutBinding;
import com.example.johnson.billscanner.entities.FlowEntity;
import com.example.johnson.billscanner.interfaces.Controller;

public class RemarksDialog extends BaseDialog {

    Button btnNext,btnCancel;
    EditText refno,remarks;
    public interface remarkListener {
        void onRemarkSuccess(String refno, String remarks);
    }

    Controller controller;
    private RemarksLayoutBinding remarksLayoutBinding;
    remarkListener remarkListener;

    @Override
    protected void initView(View view) {

        btnNext = view.findViewById(R.id.btn_next);
        btnCancel = view.findViewById(R.id.btn_cancel);
        refno = view.findViewById(R.id.name);
        remarks = view.findViewById(R.id.address);
        setActionListener();
    }

    @Override
    protected int getLayout() {
        return R.layout.remarks_layout;
    }

    public static RemarksDialog newInstance(Controller controller, remarkListener remarkListener) {

        RemarksDialog typeDialog = new RemarksDialog();
        typeDialog.controller = controller;
//        typeDialog.remarksLayoutBinding = DataBindingUtil.setContentView(controller.getBaseActivity(), R.layout.remarks_layout);
        typeDialog.remarkListener = remarkListener;
        return typeDialog;

    }

    /**
     * Default method
     *
     * @param savedInstanceState
     * @return
     */
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(controller.getBaseActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }


    private void setActionListener() {
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(refno.getText().toString())) {
                    refno.setError("add reference no");
                } else if (TextUtils.isEmpty(remarks.getText().toString())) {
                    remarks.setError("add remarks");
                } else {
                    dismiss();
                    remarkListener.onRemarkSuccess(remarks.getText().toString().trim(), refno.getText().toString().trim());
                }
            }
        });

       btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                remarkListener.onRemarkSuccess(null,null);
                dismiss();
            }
        });

    }
}
