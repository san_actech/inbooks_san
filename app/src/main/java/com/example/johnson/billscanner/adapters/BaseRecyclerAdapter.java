package com.example.johnson.billscanner.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.example.johnson.billscanner.viewholders.BaseViewHolder;

import java.util.List;

/**
 * Created on 20/06/2016.
 */
public class BaseRecyclerAdapter<VH extends BaseViewHolder> extends RecyclerView.Adapter<VH> {
	protected boolean			   showLoader;
	List<?>						   entityList;
	AdapterDelegatesManager<VH>	   delegatesManager;
	private ClickInterfaceListener listener;

	@SuppressWarnings("WeakerAccess")
	public BaseRecyclerAdapter (List<?> entityList) {
		this.entityList = entityList;
		delegatesManager = new AdapterDelegatesManager<> ();
	}

	public void setDataList (List<? > entityList) {

		this.entityList = entityList;
		notifyDataSetChanged ();
	}

	public void addAdapterDelegates (@NonNull AdapterDelegate<VH> adapterDelegate) {
		delegatesManager.addDelegate (adapterDelegate);
		delegatesManager.setFallbackDelegate(adapterDelegate);
	}

	@Override
	public int getItemViewType (int position) {
			return delegatesManager.getItemViewType(entityList, position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public VH onCreateViewHolder (ViewGroup parent, int viewType) {
		return delegatesManager.onCreateViewHolder (parent, viewType);
	}

	@Override
	public void onBindViewHolder (final VH holder, int position) {

		delegatesManager.onBindViewHolder (entityList, position, holder);
		holder.itemView.setOnClickListener (new View.OnClickListener () {
			@Override
			public void onClick (View v) {

				holder.onClick (v);

				if (holder.getAdapterPosition () == -1)
					return;

				if (listener != null)
					listener.onViewClicked (holder.itemView,
							entityList.get (holder.getAdapterPosition ()), holder.getAdapterPosition ());
			}
		});

		/*
		 * holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
		 *
		 * @Override public boolean onLongClick(View v) { holder.onLongClick(v); if
		 * (listener != null) listener.onViewClicked( holder.itemView,
		 * entityList.get(holder.getAdapterPosition()), holder.getAdapterPosition() ); }
		 * });
		 */

	}

	@Override
	public int getItemCount () {

		if (entityList.size() ==  0) {
			return 0;
		}

		return entityList.size ();
	}

	public void setClickInterface (ClickInterfaceListener listener) {

		this.listener = listener;
	}

	public interface ClickInterfaceListener {
		void onViewClicked(View holder, Object entity, int position);
	}

	public List<?> getList () {

		return entityList;
	}

    public void showLoading (boolean status) {

        showLoader = status;
    }

    public void removeAllItems () {

        entityList.clear ();
        notifyDataSetChanged ();
    }

    public void removeItem(Object o, int position) {

        entityList.remove (o);
        notifyItemRemoved (position);
    }

    public boolean shouldShowHeader () {

        return false;
    }

}
