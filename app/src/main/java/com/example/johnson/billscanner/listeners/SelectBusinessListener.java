package com.example.johnson.billscanner.listeners;

import com.example.johnson.billscanner.entities.BusinessEntity;

public interface SelectBusinessListener {

    void OnBusinessClick(BusinessEntity businessEntity);

}
