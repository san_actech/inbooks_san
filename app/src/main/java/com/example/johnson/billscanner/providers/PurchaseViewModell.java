package com.example.johnson.billscanner.providers;

import androidx.lifecycle.ViewModel;

import com.example.johnson.billscanner.entities.FinalDataModel;
import com.example.johnson.billscanner.entities.FlowEntity;
import com.example.johnson.billscanner.interfaces.Constansts;
import com.example.johnson.billscanner.listeners.DataListener;
import com.example.johnson.billscanner.utils.FButils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Calendar;

public class PurchaseViewModell extends ViewModel {


    public void uploadData(FlowEntity flowEntity, final DataListener dataListener) {


        FinalDataModel finalDataModel = new FinalDataModel();

        finalDataModel.setAccountType(flowEntity.getAccountType());
        finalDataModel.setBusinessId(flowEntity.getBusinessId());
        finalDataModel.setCreatedOn(Calendar.getInstance().getTimeInMillis());
        finalDataModel.setDocuments(flowEntity.getDocuments());
        finalDataModel.setFinancialMonth(flowEntity.getFinancialMonth());
        finalDataModel.setRefNo(flowEntity.getRefNo());
        finalDataModel.setRemarks(flowEntity.getRemarks());
        finalDataModel.setImages(flowEntity.getImages());
        finalDataModel.setVendorsId(flowEntity.getVendorsId());
        finalDataModel.setModeOfTransaction(flowEntity.getModeOfTransaction());
        finalDataModel.setProcessed(0);
        finalDataModel.setStatus(0);

        DocumentReference documentReference = FButils.getDocumentReferenceForCompents(flowEntity.getComponents(),flowEntity.getBusinessId(),flowEntity.getFinancialYear());

        documentReference.collection("DATA").document(FButils.getDocumentid(documentReference.collection("DATA"))).set(finalDataModel).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dataListener.onItemUpdated("", null);
            }
        });
    }
}
