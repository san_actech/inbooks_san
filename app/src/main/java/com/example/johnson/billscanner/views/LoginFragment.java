package com.example.johnson.billscanner.views;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.johnson.billscanner.R;
import com.example.johnson.billscanner.customfonts.InBookEditText;
import com.example.johnson.billscanner.listeners.LoginActionListener;
import com.example.johnson.billscanner.utils.Loader;

public class LoginFragment extends Fragment {

    Button btnLogin;
    LoginActionListener loginActionListener;
    InBookEditText inBookEditText;

    public static LoginFragment newInstance(LoginActionListener loginActionListener) {
        LoginFragment fragment = new LoginFragment();
        fragment.loginActionListener = loginActionListener;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View loginView = inflater.inflate(R.layout.fragment_login, container, false);
        btnLogin = loginView.findViewById(R.id.sinnp);
        inBookEditText = loginView.findViewById(R.id.etd_phone);
        setActionListener();
        return loginView;
    }


    private void setActionListener() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(inBookEditText.getText().toString())) {
                    inBookEditText.setError("Enter your phone number");
                } else {

                    Loader.showLoader(getActivity(),"Loading","Please wait");
                    loginActionListener.onGetCodeClick("+91" + inBookEditText.getText().toString());
                }
            }
        });
    }
}
