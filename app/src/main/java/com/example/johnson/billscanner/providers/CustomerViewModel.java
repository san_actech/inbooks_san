package com.example.johnson.billscanner.providers;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;

import com.example.johnson.billscanner.entities.BusinessEntity;
import com.example.johnson.billscanner.entities.CustomerEntity;
import com.example.johnson.billscanner.entities.CustomerList;
import com.example.johnson.billscanner.entities.VendorEntity;
import com.example.johnson.billscanner.entities.VendorsList;
import com.example.johnson.billscanner.listeners.DataListener;
import com.example.johnson.billscanner.utils.FButils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class CustomerViewModel extends ViewModel {


    public void getCustomer(String userId,String id, final DataListener<CustomerEntity> dataListener){

        FirebaseFirestore.getInstance().collection(FButils.TB_NAMES.USER_INSTANCE).document(userId).collection(FButils.TB_NAMES.CUSTOMER_INSTANCE).document(id).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {

                dataListener.onItemUpdated(documentSnapshot.getId(),documentSnapshot.toObject(CustomerEntity.class));

            }
        });
    }


    public void addNewCustomer(String userId,CustomerEntity businessEntity, final DataListener<CustomerEntity> dataListener){

        FirebaseFirestore.getInstance().collection(FButils.TB_NAMES.USER_INSTANCE).document(userId).collection(FButils.TB_NAMES.CUSTOMER_INSTANCE).add(businessEntity).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {

                dataListener.onItemAdded(documentReference.getId(),null);
            }
        });
    }

    public void getBusinessForUser(String id,String businessId, final DataListener<CustomerEntity> dataListener){

        FirebaseFirestore.getInstance().collection(FButils.TB_NAMES.BUSINESS_INSTANCE).whereEqualTo("userId",id).whereEqualTo("businesssId",businessId).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                if (task.isSuccessful()){

                }
            }
        });
    }


    public void getCustomerList(String id, String businessId, final DataListener<CustomerList> dataListener) {

        final List<CustomerEntity> customerEntities = new ArrayList<>();

        FirebaseFirestore.getInstance().collection(FButils.TB_NAMES.USER_INSTANCE).document(id).collection(FButils.TB_NAMES.CUSTOMER_INSTANCE).whereEqualTo("businessId", businessId).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                if (task.isSuccessful() && !task.getResult().getDocuments().isEmpty()) {

                    for (DocumentSnapshot snapshot : task.getResult().getDocuments()) {

                        CustomerEntity customerEntity = snapshot.toObject(CustomerEntity.class);
                        if (customerEntity != null) {
                            customerEntity.setId(snapshot.getId());
                            customerEntities.add(customerEntity);
                        }
                    }
                }

                if (customerEntities.size() > 0) {
                    CustomerList vendorsList = new CustomerList();
                    vendorsList.setCustomerEntities(customerEntities);
                    dataListener.onItemUpdated("", vendorsList);
                } else {
                    dataListener.onItemUpdated("", null);
                }
            }
        });
    }


}
