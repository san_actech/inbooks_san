package com.example.johnson.billscanner.adapters;

import android.app.Activity;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.johnson.billscanner.R;
import com.example.johnson.billscanner.listeners.GridClickListener;
import com.example.johnson.billscanner.utils.DocumentImageUtilModel;
import com.mzelzoghbi.zgallery.ImageViewHolder;

import java.util.List;

/**
 * Created by mohamedzakaria on 8/7/16.
 */
public class GridImagesAdapter extends RecyclerView.Adapter<ImageViewHolder> {
    private List<DocumentImageUtilModel> imageURLs;
    private Activity mActivity;
    private int imgPlaceHolderResId = -1;
    private GridClickListener clickListener;

    public GridImagesAdapter(GridClickListener listener, Activity activity, List<DocumentImageUtilModel> imageURLs) {
        this.imageURLs = imageURLs;
        this.mActivity = activity;
        this.clickListener = listener;
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ImageViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.z_item_image, null));
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, final int position) {
        RequestOptions requestOptions = new RequestOptions().placeholder(R.drawable.belvedere_ic_camera);
        Glide.with(mActivity).load(imageURLs.get(position).getImageStream())
                .apply(requestOptions)
                .into(holder.image);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListener.onClick(position);
            }
        });
    }


    public void setlist(List<DocumentImageUtilModel> imageURLs){
        this.imageURLs = imageURLs;
        notifyDataSetChanged();

    }


    @Override
    public int getItemCount() {
        return imageURLs != null ? imageURLs.size() : 0;
    }
}
