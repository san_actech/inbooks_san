package com.example.johnson.billscanner.listeners;

import com.example.johnson.billscanner.entities.DashboarEntity;

public interface DashboarItemClickListener {

    void onItemClick(DashboarEntity dashboarEntity);
}
