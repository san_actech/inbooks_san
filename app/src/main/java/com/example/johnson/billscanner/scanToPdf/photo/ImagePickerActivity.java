package com.example.johnson.billscanner.scanToPdf.photo;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.johnson.billscanner.R;
import com.example.johnson.billscanner.scanToPdf.photo.model.Image;
import com.example.johnson.billscanner.scanToPdf.photo.utils.ImageInternalFetcher;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;


public class ImagePickerActivity extends AppCompatActivity {

    private static final String KEY_LIST = "savedinstance.key.list";

    public static final String EXTRA_IMAGE_URIS = "selected_image_uris";
    public static final String EXTRA_IMAGE_ORIENTATIONS = "selected_image_orientations";

    private static final int INTENT_REQUEST_GET_IMAGES = 13;
    private static final int INTENT_REQUEST_GET_N_IMAGES = 14;

    private Set<Image> mSelectedImages;
    private LinearLayout mSelectedImagesContainer;
    protected TextView mSelectedImageEmptyMessage;

    private ViewPager mViewPager;
    public ImageInternalFetcher mImageFetcher;

    private Button mCancelButtonView, mDoneButtonView;

    private SlidingTabText mSlidingTabText;

    private static Config mConfig = new Config.Builder().build();

    public static void setConfig(Config config) {

        if (config == null) {
            throw new NullPointerException("Config cannot be passed null. Not setting config will use default values.");
        }
        mConfig = config;
    }

    public static Config getConfig() {
        return mConfig;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scan_to_pdf_activity);

        mSelectedImagesContainer = (LinearLayout) findViewById(R.id.pp__selected_photos_container);
        mSelectedImageEmptyMessage = (TextView) findViewById(R.id.pp__selected_photos_empty);
        mViewPager = (ViewPager) findViewById(R.id.pp__pager);
        mCancelButtonView = (Button) findViewById(R.id.pp__btn_cancel);
        mDoneButtonView = (Button) findViewById(R.id.pp__btn_done);

        mSelectedImages = new HashSet<Image>();
        mImageFetcher = new ImageInternalFetcher(this, 500);

        mCancelButtonView.setOnClickListener(mOnFinishGettingImages);
        mDoneButtonView.setOnClickListener(mOnFinishGettingImages);

        setupActionBar();
        if (savedInstanceState != null) {
            populateUi(savedInstanceState);
        }
    }

    private void populateUi(Bundle savedInstanceState) {
        ArrayList<Image> list = savedInstanceState.getParcelableArrayList(KEY_LIST);

        if (list != null) {
            for (Image image : list) {
                addImage(image);
            }
        }
    }

    private void setupActionBar() {
        mSlidingTabText = (SlidingTabText) findViewById(R.id.pp__sliding_tabs);
        mSlidingTabText.setSelectedIndicatorColors(getResources().getColor(mConfig.getTabSelectionIndicatorColor()));
        mSlidingTabText.setCustomTabView(R.layout.pp__tab_view_text, R.id.pp_tab_text);
        mSlidingTabText.setTabStripColor(mConfig.getTabBackgroundColor());
        mViewPager.setAdapter(new PagerAdapter2Fragments(getSupportFragmentManager()));
        mSlidingTabText.setTabTitles(getResources().getStringArray(R.array.tab_titles));
        mSlidingTabText.setViewPager(mViewPager);
    }

    public boolean addImage(Image image) {

        if (mSelectedImages == null) {
            mSelectedImages = new HashSet<Image>();
        }

        if (mSelectedImages.size() == mConfig.getSelectionLimit()) {
            Toast.makeText(this, "no Image selected" + mConfig.getSelectionLimit(), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            if (mSelectedImages.add(image)) {
                View rootView = LayoutInflater.from(ImagePickerActivity.this).inflate(R.layout.pp__list_item_selected_thumbnail, null);
                ImageView thumbnail = (ImageView) rootView.findViewById(R.id.pp__selected_photo);
                rootView.setTag(image.mUri);
                mImageFetcher.loadImage(image.mUri, thumbnail, image.mOrientation);
                mSelectedImagesContainer.addView(rootView, 0);

                int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60, getResources().getDisplayMetrics());
                thumbnail.setLayoutParams(new FrameLayout.LayoutParams(px, px));

                if (mSelectedImages.size() >= 1) {
                    mSelectedImagesContainer.setVisibility(View.VISIBLE);
                    mSelectedImageEmptyMessage.setVisibility(View.GONE);
                }
                return true;
            }
        }
        return false;
    }

    public boolean removeImage(Image image) {
        if (mSelectedImages.remove(image)) {
            for (int i = 0; i < mSelectedImagesContainer.getChildCount(); i++) {
                View childView = mSelectedImagesContainer.getChildAt(i);
                if (childView.getTag().equals(image.mUri)) {
                    mSelectedImagesContainer.removeViewAt(i);
                    break;
                }
            }

            if (mSelectedImages.size() == 0) {
                mSelectedImagesContainer.setVisibility(View.GONE);
                mSelectedImageEmptyMessage.setVisibility(View.VISIBLE);
            }
            return true;
        }
        return false;
    }

    public boolean containsImage(Image image) {
        return mSelectedImages.contains(image);
    }

    private View.OnClickListener mOnFinishGettingImages = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.pp__btn_done) {
                if (mSelectedImages !=null && mSelectedImages.size()>0) {
                    Uri[] uris = new Uri[mSelectedImages.size()];
                    int[] orientations = new int[mSelectedImages.size()];
                    int i = 0;
                    for (Image img : mSelectedImages) {
                        uris[i] = img.mUri;
                        orientations[i++] = img.mOrientation;
                    }
                    Intent intent = new Intent();
                    intent.putExtra(EXTRA_IMAGE_URIS, uris);
                    intent.putExtra(EXTRA_IMAGE_ORIENTATIONS, orientations);
                    setResult(Activity.RESULT_OK, intent);
                } else {
                    Toast.makeText(getBaseContext(),"no photo clicked",Toast.LENGTH_SHORT).show();
                }
            } else
            if (view.getId() == R.id.pp__btn_cancel) {
                setResult(Activity.RESULT_CANCELED);
            }

            finish();
        }
    };

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        ArrayList<Image> list = new ArrayList<Image>(mSelectedImages);
        outState.putParcelableArrayList(KEY_LIST, list);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        populateUi(savedInstanceState);
    }
}