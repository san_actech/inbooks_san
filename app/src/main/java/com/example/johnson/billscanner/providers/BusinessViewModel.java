package com.example.johnson.billscanner.providers;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;

import com.example.johnson.billscanner.entities.BusinessEntity;
import com.example.johnson.billscanner.entities.BusinessList;
import com.example.johnson.billscanner.listeners.DataListener;
import com.example.johnson.billscanner.utils.FButils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class BusinessViewModel extends ViewModel {



    public void getBusiness(String userId,String id, final DataListener<BusinessEntity> dataListener){

        FirebaseFirestore.getInstance().collection(FButils.TB_NAMES.USER_INSTANCE).document(userId).collection(FButils.TB_NAMES.BUSINESS_INSTANCE).document(id).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {

                dataListener.onItemUpdated(documentSnapshot.getId(),documentSnapshot.toObject(BusinessEntity.class));

            }
        });
    }


    public void addNewBusiness(String userId,BusinessEntity businessEntity, final DataListener<BusinessEntity> dataListener){

        FirebaseFirestore.getInstance().collection(FButils.TB_NAMES.USER_INSTANCE).document(userId).collection(FButils.TB_NAMES.BUSINESS_INSTANCE).add(businessEntity).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {

                dataListener.onItemAdded(documentReference.getId(),null);
            }
        });
    }


    public void getBusinessForUser(String id, final DataListener<BusinessList> dataListener){

        final List<BusinessEntity> businessEntities = new ArrayList<>();
        FirebaseFirestore.getInstance().collection(FButils.TB_NAMES.USER_INSTANCE).document(id).collection(FButils.TB_NAMES.BUSINESS_INSTANCE).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                if (task.isSuccessful()){

                }

                if (task.isSuccessful() && !task.getResult().getDocuments().isEmpty()) {

                    for (DocumentSnapshot snapshot : task.getResult().getDocuments()) {

                        BusinessEntity businessEntity = snapshot.toObject(BusinessEntity.class);
                        if (businessEntity != null) {
                            businessEntity.setBusinessId(snapshot.getId());
                            businessEntities.add(businessEntity);
                        }
                    }
                }

                if (businessEntities.size() > 0) {
                    BusinessList vendorsList = new BusinessList();
                    vendorsList.setBusinessEntities(businessEntities);
                    dataListener.onItemUpdated("", vendorsList);
                } else {
                    dataListener.onItemUpdated("", null);
                }


            }
        });
    }

}
