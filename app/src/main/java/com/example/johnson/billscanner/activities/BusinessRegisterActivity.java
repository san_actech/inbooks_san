package com.example.johnson.billscanner.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import androidx.lifecycle.ViewModelProviders;

import com.example.johnson.billscanner.entities.BusinessEntity;
import com.example.johnson.billscanner.interfaces.Constansts;
import com.example.johnson.billscanner.interfaces.Controller;
import com.example.johnson.billscanner.listeners.DataListener;
import com.example.johnson.billscanner.providers.BusinessViewModel;
import com.example.johnson.billscanner.utils.Loader;
import com.example.johnson.billscanner.utils.PermissionUtils;
import com.example.johnson.billscanner.utils.PreferenceUtil;
import com.example.johnson.billscanner.views.BaseView;
import com.example.johnson.billscanner.views.BusinessRegisterActivityView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import zendesk.belvedere.Belvedere;
import zendesk.belvedere.Callback;
import zendesk.belvedere.MediaResult;

public class BusinessRegisterActivity extends BaseActivity {

    private InputStream inputStream;

    private String isFrom = "";

    @Override
    protected BaseView getViewForController(Controller controller) {

        return new BusinessRegisterActivityView(controller);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Belvedere.from(this).getFilesFromActivityOnResult(requestCode, resultCode, data, new Callback<List<MediaResult>>() {
            @Override
            public void success(List<MediaResult> results) {

                if (results != null && !results.isEmpty()) {
                    MediaResult result = results.get(0);

                    if (result != null) {
                        File selectedImage = result.getFile();
                        String photoFilePath = selectedImage.getAbsolutePath();
                        if (photoFilePath != null && !photoFilePath.isEmpty()) {
                            Uri inputUri = Uri.fromFile(new File(photoFilePath));

                            ((BusinessRegisterActivityView) view).loadPhoto(inputUri);
//
//                            Picasso.with(userLandingActivity)
//                                    .load(inputUri)
//                                    .placeholder(R.drawable.noproduct)
//                                    .error(R.drawable.noproduct)
//                                    .into(fragmentProductAddBinding.productImage);
                            try {
                                inputStream = getContentResolver().openInputStream(inputUri);
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(BusinessRegisterActivity.this, "Unable to fetch selected Photo", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(BusinessRegisterActivity.this, "Unable to fetch selected Photo", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }


    public void storeBusiness(BusinessEntity businessEntity) {

        Loader.showLoader(this,"Loading","Please wait");
        String userId = PreferenceUtil.getString(this, Constansts.USER_KEY, "");

        BusinessViewModel businessViewModel = ViewModelProviders.of(this).get(BusinessViewModel.class);

        businessViewModel.addNewBusiness(userId, businessEntity, new DataListener<BusinessEntity>() {
            @Override
            public void onItemAdded(String key, BusinessEntity data) {

                Loader.hideLoader();
                PreferenceUtil.saveString(BusinessRegisterActivity.this, Constansts.BUSINESS_KEY, key);

                startActivity(new Intent(BusinessRegisterActivity.this, MainActivity.class));
            }

            @Override
            public void onItemUpdated(String key, BusinessEntity updatedData) {

            }

            @Override
            public void onItemDeleted(String deletedItemKey) {

            }

            @Override
            public void onFailed(int errorCode, String errorMsg) {

            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (PermissionUtils.getInstance(this).hasCameraPermission()) {

        } else {
            return;
        }
    }
}
