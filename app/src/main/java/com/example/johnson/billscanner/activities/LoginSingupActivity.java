package com.example.johnson.billscanner.activities;

import android.content.Intent;
import androidx.annotation.NonNull;

import com.example.johnson.billscanner.utils.Loader;
import com.google.android.material.snackbar.Snackbar;
import android.util.Log;
import android.view.View;

import com.example.johnson.billscanner.interfaces.Controller;
import com.example.johnson.billscanner.listeners.LoginActionListener;
import com.example.johnson.billscanner.views.BaseView;
import com.example.johnson.billscanner.views.LoginSIgnupViews;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

public class LoginSingupActivity extends BaseActivity implements LoginActionListener {

    FirebaseAuth mAuth = FirebaseAuth.getInstance();

    @Override
    protected BaseView getViewForController(Controller controller) {

        if (FirebaseAuth.getInstance().getCurrentUser() != null){
            startMainActivity();
        }
        return new LoginSIgnupViews(controller);
    }

    @Override
    public void setSnackbar(Snackbar snackBar) {

    }

    @Override
    public void onGetCodeClick(String mobileNo) {

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                mobileNo,
                60,
                TimeUnit.SECONDS,
                this,
                mCallbacks);
    }

    private void startMainActivity(){
        startActivity(new Intent(this,MainActivity.class));
    }

    @Override
    public void onVerifyClick(String Code) {

//        startActivity(new Intent(LoginSingupActivity.this, UserRegisterActivity.class));

        Loader.showLoader(this,"Loading","Please wait");
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, Code);
        signInWithPhoneAuthCredential(credential);
    }

    @Override
    public void onResendClick() {

    }


    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private final String TAG = "LOGIN_ACTIVITY";

    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onVerificationCompleted(PhoneAuthCredential credential) {
            Log.d(TAG, "onVerificationCompleted:" + credential);

            signInWithPhoneAuthCredential(credential);
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Log.w(TAG, "onVerificationFailed", e);
            showSnackError(e.getLocalizedMessage());
            Loader.hideLoader();

            if (e instanceof FirebaseAuthInvalidCredentialsException) {

            } else if (e instanceof FirebaseTooManyRequestsException) {

            }
        }

        @Override
        public void onCodeSent(String verificationId,
                               PhoneAuthProvider.ForceResendingToken token) {
            Log.d(TAG, "onCodeSent:" + verificationId);
            mVerificationId = verificationId;
            mResendToken = token;
            Loader.hideLoader();
            ((LoginSIgnupViews) view).setPage(1);

        }
    };


    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = task.getResult().getUser();
                            Loader.hideLoader();
                            startActivity(new Intent(LoginSingupActivity.this, UserRegisterActivity.class));

                        } else {
                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {

                                showSnackError("try again");

                            }
                        }
                    }
                });
    }

    private void showSnackError(String errorMsg) {
        Snackbar snackbar = Snackbar
                .make(view.getView(), errorMsg, Snackbar.LENGTH_LONG);
        snackbar.show();
    }


}
