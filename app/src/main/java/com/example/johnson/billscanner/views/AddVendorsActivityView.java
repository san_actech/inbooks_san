package com.example.johnson.billscanner.views;

import android.text.TextUtils;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.example.johnson.billscanner.R;
import com.example.johnson.billscanner.activities.AddVendorsActivity;
import com.example.johnson.billscanner.activities.VendorsActivity;
import com.example.johnson.billscanner.databinding.AddVendorsBinding;
import com.example.johnson.billscanner.entities.CustomerEntity;
import com.example.johnson.billscanner.entities.VendorEntity;
import com.example.johnson.billscanner.interfaces.Constansts;
import com.example.johnson.billscanner.interfaces.Controller;
import com.example.johnson.billscanner.listeners.DataListener;
import com.example.johnson.billscanner.providers.CustomerViewModel;
import com.example.johnson.billscanner.providers.VendorsViewModell;
import com.example.johnson.billscanner.utils.Loader;
import com.example.johnson.billscanner.utils.PreferenceUtil;
import com.google.firebase.auth.FirebaseAuth;

public class AddVendorsActivityView extends BaseView {

    private AddVendorsBinding addVendorsBinding;

    public AddVendorsActivityView(Controller controller) {
        super(controller);
    }

    @Override
    protected int getViewLayout() {
        return R.layout.add_vendors;
    }

    @Override
    protected void onCreate() {

        addVendorsBinding = DataBindingUtil.setContentView(controller.getBaseActivity(), R.layout.add_vendors);


        switch (((AddVendorsActivity) controller.getBaseActivity()).getFlowEntity().getComponents()){
            case SALES:
                addVendorsBinding.txtViewTitle.setText("ADD CUSTOMER");
                break;
            case PURCHASE:
                addVendorsBinding.txtViewTitle.setText("ADD VENDORS");
                break;
        }

    }

    @Override
    protected void setActionListeners() {

        addVendorsBinding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validate()) {
                    Loader.showLoader(controller.getBaseActivity(), "Loading", "Please wait");

                    switch (((AddVendorsActivity) controller.getBaseActivity()).getFlowEntity().getComponents()){
                        case SALES:

                            CustomerViewModel customerViewModel = ViewModelProviders.of(controller.getBaseActivity()).get(CustomerViewModel.class);
                            CustomerEntity customerEntity = new CustomerEntity(addVendorsBinding.name.getText().toString(), addVendorsBinding.address.getText().toString(), addVendorsBinding.gst.getText().toString()
                                    , addVendorsBinding.pan.getText().toString(), addVendorsBinding.email.getText().toString(), addVendorsBinding.phone.getText().toString(), "",
                                    PreferenceUtil.getString(controller.getBaseActivity(), Constansts.BUSINESS_KEY, ""));

                            customerViewModel.addNewCustomer(FirebaseAuth.getInstance().getCurrentUser().getUid(), customerEntity, new DataListener<CustomerEntity>() {
                                @Override
                                public void onItemAdded(String key, CustomerEntity data) {
                                    Loader.hideLoader();
                                    controller.getBaseActivity().finish();
                                }

                                @Override
                                public void onItemUpdated(String key, CustomerEntity updatedData) {

                                }

                                @Override
                                public void onItemDeleted(String deletedItemKey) {

                                }

                                @Override
                                public void onFailed(int errorCode, String errorMsg) {

                                }
                            });

                            break;
                        case PURCHASE:

                            VendorsViewModell vendorsViewModell = ViewModelProviders.of(controller.getBaseActivity()).get(VendorsViewModell.class);
                            VendorEntity vendorEntity = new VendorEntity(addVendorsBinding.name.getText().toString(), addVendorsBinding.address.getText().toString(), addVendorsBinding.gst.getText().toString()
                                    , addVendorsBinding.pan.getText().toString(), addVendorsBinding.email.getText().toString(), addVendorsBinding.phone.getText().toString(), "",
                                    PreferenceUtil.getString(controller.getBaseActivity(), Constansts.BUSINESS_KEY, ""));

                            vendorsViewModell.addNewVendor(FirebaseAuth.getInstance().getCurrentUser().getUid(), vendorEntity, new DataListener<VendorEntity>() {
                                @Override
                                public void onItemAdded(String key, VendorEntity data) {
                                    Loader.hideLoader();
                                    controller.getBaseActivity().finish();
                                }

                                @Override
                                public void onItemUpdated(String key, VendorEntity updatedData) {

                                }

                                @Override
                                public void onItemDeleted(String deletedItemKey) {

                                }

                                @Override
                                public void onFailed(int errorCode, String errorMsg) {

                                }
                            });

                            break;
                    }

                }            }
        });
    }


    private boolean validate() {
        if (TextUtils.isEmpty(addVendorsBinding.name.getText().toString().trim())) {
            addVendorsBinding.name.setError("Enter business name");
            return false;
        } else if (TextUtils.isEmpty(addVendorsBinding.phone.getText().toString())) {
            addVendorsBinding.phone.setError("Enter phone number");
            return false;
        } else if (TextUtils.isEmpty(addVendorsBinding.address.getText().toString())) {
            addVendorsBinding.address.setError("Enter address");
            return false;
        } else if (TextUtils.isEmpty(addVendorsBinding.email.getText().toString())) {
            addVendorsBinding.email.setError("Enter your email");
            return false;
        } else if (TextUtils.isEmpty(addVendorsBinding.pan.getText().toString())) {
            addVendorsBinding.pan.setError("Enter PAN number");
            return false;
        } else if (TextUtils.isEmpty(addVendorsBinding.gst.getText().toString())) {
            addVendorsBinding.gst.setError("Enter GST number");
            return false;
        }

        return true;
    }
}
