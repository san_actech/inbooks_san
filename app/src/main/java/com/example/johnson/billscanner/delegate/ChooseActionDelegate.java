package com.example.johnson.billscanner.delegate;

import android.view.View;

import androidx.annotation.NonNull;

import com.example.johnson.billscanner.R;
import com.example.johnson.billscanner.adapters.AdapterDelegate;
import com.example.johnson.billscanner.interfaces.Controller;
import com.example.johnson.billscanner.listeners.ChooseActionListener;
import com.example.johnson.billscanner.viewholders.ChooseActionViewHolder;

import java.util.List;

public class ChooseActionDelegate implements AdapterDelegate<ChooseActionViewHolder> {


    Controller controller;
    ChooseActionListener chooseVendorsActionClickLIstener;

    public ChooseActionDelegate(Controller controller, ChooseActionListener chooseVendorsActionClickLIstener) {
        this.controller = controller;
        this.chooseVendorsActionClickLIstener = chooseVendorsActionClickLIstener;

    }

    @Override
    public boolean isForViewType(@NonNull List<?> items, int position) {
        return true;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.choose_action_layout;
    }

    @NonNull
    @Override
    public ChooseActionViewHolder getViewHolder(View itemView) {
        return new ChooseActionViewHolder(itemView,controller, chooseVendorsActionClickLIstener);
    }
}