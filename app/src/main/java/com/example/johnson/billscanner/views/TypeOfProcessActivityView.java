package com.example.johnson.billscanner.views;

import android.content.Intent;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.johnson.billscanner.R;
import com.example.johnson.billscanner.activities.TypeOfProcessActivity;
import com.example.johnson.billscanner.activities.VendorsActivity;
import com.example.johnson.billscanner.adapters.BaseRecyclerAdapter;
import com.example.johnson.billscanner.databinding.TypeOfProcessLayoutBinding;
import com.example.johnson.billscanner.delegate.ChooseActionDelegate;
import com.example.johnson.billscanner.delegate.ChooseVendorDelegate;
import com.example.johnson.billscanner.entities.ChooseActionEntity;
import com.example.johnson.billscanner.entities.FlowEntity;
import com.example.johnson.billscanner.interfaces.Constansts;
import com.example.johnson.billscanner.interfaces.Controller;
import com.example.johnson.billscanner.listeners.ChooseActionListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class TypeOfProcessActivityView extends BaseView implements ChooseActionListener {

    private TypeOfProcessLayoutBinding binding;

    private BaseRecyclerAdapter mAdapter;

    private ChooseActionEntity chooseActionEntity;

    public TypeOfProcessActivityView(Controller controller) {
        super(controller);
    }

    @Override
    protected int getViewLayout() {
        return R.layout.type_of_process_layout;
    }

    @Override
    protected void onCreate() {

        binding = DataBindingUtil.setContentView(controller.getBaseActivity(), R.layout.type_of_process_layout);
        setListInAdapter();
    }

    @Override
    protected void setActionListeners() {

    }

    private void setListInAdapter() {
        try {
            LinearLayoutManager layoutManager = new LinearLayoutManager(controller.getBaseActivity());
            binding.rvType.setLayoutManager(layoutManager);
            mAdapter = new BaseRecyclerAdapter(getList());
            mAdapter.addAdapterDelegates(new ChooseActionDelegate(controller, this));
            binding.rvType.setAdapter(mAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<ChooseActionEntity> getList() {

        List<ChooseActionEntity> dashboarEntities = new ArrayList<>();

        switch (((TypeOfProcessActivity) controller.getBaseActivity()).getFlowEntity().getComponents()) {
            case PURCHASE:

                dashboarEntities.add(new ChooseActionEntity("CASH PURCHASE", false));
                dashboarEntities.add(new ChooseActionEntity("BANK PURCHASE", false));
                dashboarEntities.add(new ChooseActionEntity("CREDIT PURCHASE", false));
                dashboarEntities.add(new ChooseActionEntity("CONSIGNMENT PURCHASE", false));
                dashboarEntities.add(new ChooseActionEntity("DC PURCHASE", false));
                break;
            case EXPENSES:
                dashboarEntities.add(new ChooseActionEntity("CASH EXPENSE", false));
                dashboarEntities.add(new ChooseActionEntity("BANK EXPENSE", false));
                dashboarEntities.add(new ChooseActionEntity("PENDING EXPENSE", false));
                break;
            case PENDING:
                break;
            case INCOME:
                dashboarEntities.add(new ChooseActionEntity("CASH INCOME", false));
                dashboarEntities.add(new ChooseActionEntity("BANK INCOME", false));
                dashboarEntities.add(new ChooseActionEntity("PENDING INCOME", false));
                break;
            case ASSETS:
                dashboarEntities.add(new ChooseActionEntity("CASH ASSET", false));
                dashboarEntities.add(new ChooseActionEntity("BANK ASSET", false));
                dashboarEntities.add(new ChooseActionEntity("PENDING ASSET", false));
                break;
            case SALES:
                dashboarEntities.add(new ChooseActionEntity("CASH SALES", false));
                dashboarEntities.add(new ChooseActionEntity("BANK SALES", false));
                dashboarEntities.add(new ChooseActionEntity("CREDIT SALES", false));
                dashboarEntities.add(new ChooseActionEntity("CONSIGNMENT SALES", false));
                dashboarEntities.add(new ChooseActionEntity("DC SALES", false));

                break;
            case LOANS:
                dashboarEntities.add(new ChooseActionEntity("CASH LIABILITY", false));
                dashboarEntities.add(new ChooseActionEntity("BANK LIABILITY", false));
                dashboarEntities.add(new ChooseActionEntity("PENDING LIABILITY", false));

                break;
        }
        return dashboarEntities;
    }


    @Override
    public void onItemClick(ChooseActionEntity chooseActionEntity) {

        this.chooseActionEntity = chooseActionEntity;


        FlowEntity flowEntity = ((TypeOfProcessActivity) controller.getBaseActivity()).getFlowEntity();
        flowEntity.setModeOfTransaction(chooseActionEntity.getKey());

        Intent intent = new Intent(controller.getBaseActivity(), VendorsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(Constansts.ARG_FLOW, new Gson().toJson(flowEntity));
        intent.putExtras(bundle);
        getBaseActivity().startActivity(intent);

    }
}
