package com.example.johnson.billscanner.listeners;

import com.example.johnson.billscanner.entities.ChooseActionEntity;

public interface ChooseActionListener {

    void onItemClick(ChooseActionEntity chooseActionEntity);
}
