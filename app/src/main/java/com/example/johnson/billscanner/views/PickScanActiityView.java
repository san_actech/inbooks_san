package com.example.johnson.billscanner.views;

import android.net.Uri;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import com.example.johnson.billscanner.R;
import com.example.johnson.billscanner.activities.BaseActivity;
import com.example.johnson.billscanner.activities.PickScanActivity;
import com.example.johnson.billscanner.adapters.GridImagesAdapter;
import com.example.johnson.billscanner.databinding.ScanlistActivityBinding;
import com.example.johnson.billscanner.dialog.RemarksDialog;
import com.example.johnson.billscanner.interfaces.Controller;
import com.example.johnson.billscanner.listeners.GridClickListener;
import com.example.johnson.billscanner.utils.DocumentImageUtilModel;
import com.example.johnson.billscanner.utils.PermissionUtils;

import java.util.List;

import zendesk.belvedere.Belvedere;
import zendesk.belvedere.BelvedereUi;
import zendesk.belvedere.MediaIntent;

public class PickScanActiityView extends BaseView implements GridClickListener {

    private ScanlistActivityBinding scanlistActivityBinding;

    private int spanCount = 2;
    private GridImagesAdapter adapter = null;

    public PickScanActiityView(Controller controller) {
        super(controller);
    }

    @Override
    protected int getViewLayout() {
        return R.layout.scanlist_activity;
    }

    @Override
    protected void onCreate() {

        scanlistActivityBinding = DataBindingUtil.setContentView(controller.getBaseActivity(), R.layout.scanlist_activity);

    }

    public void updateList(List<DocumentImageUtilModel> imageStream) {

        if (adapter == null) {
            adapter = new GridImagesAdapter(this,controller.getBaseActivity(), imageStream);
            scanlistActivityBinding.recyclerView.setLayoutManager(new GridLayoutManager(controller.getBaseActivity(), 3));
            scanlistActivityBinding.recyclerView.setAdapter(adapter);
        } else {
            adapter.setlist(imageStream);
        }
    }

    @Override
    protected void setActionListeners() {

        scanlistActivityBinding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Belvedere belvedere = Belvedere.from(controller.getBaseActivity());
//                MediaIntent document = belvedere.document().build();
//                MediaIntent camera = belvedere.camera().build();
//
//                BelvedereUi.showDialog(controller.getBaseActivity().getSupportFragmentManager(), document, camera);

//                Intent intent = new Intent(controller.getBaseActivity(), ScanActivity.class);
//                intent.putExtra(ScanConstants.OPEN_INTENT_PREFERENCE, ScanConstants.OPEN_CAMERA);
//                controller.getBaseActivity().startActivityForResult(intent, 99);



                try {

                    if (PermissionUtils.getInstance(controller.getBaseActivity()).hasCameraPermission()) {

                        Belvedere belvedere = Belvedere.from(controller.getBaseActivity());
                        MediaIntent document = belvedere.document().build();
                        MediaIntent camera = belvedere.camera().build();

                        BelvedereUi.showDialog(controller.getBaseActivity().getSupportFragmentManager(), document, camera);


                    } else {
                        PermissionUtils.getInstance(controller.getBaseActivity()).requestCameraPermission(controller.getBaseActivity());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        scanlistActivityBinding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RemarksDialog typeDialog = RemarksDialog.newInstance(getBaseActivity(), new RemarksDialog.remarkListener() {
                    @Override
                    public void onRemarkSuccess(String refno, String remarks) {
                        ((PickScanActivity)controller.getBaseActivity()).uploadProductImage(0,refno,remarks);
                    }
                });
//                typeDialog.show(controller.getBaseActivity().getSupportFragmentManager(),"");
                ((BaseActivity) getBaseActivity()).showDialog(typeDialog);


            }
        });
    }

    @Override
    public void onClick(int pos) {

    }
}
